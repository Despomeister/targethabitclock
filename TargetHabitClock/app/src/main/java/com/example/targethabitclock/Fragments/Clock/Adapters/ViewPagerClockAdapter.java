package com.example.targethabitclock.Fragments.Clock.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.targethabitclock.Fragments.Clock.ClockHomeFragment;
import com.example.targethabitclock.Fragments.Clock.ClockMorningRitualFragment;


public class ViewPagerClockAdapter extends FragmentStatePagerAdapter {

    public ViewPagerClockAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new ClockHomeFragment(null);
            case 1:
                return new ClockMorningRitualFragment(null);
        }
        return new ClockHomeFragment(null);
    }

    @Override
    public int getCount() {
        return 2;
    }
}