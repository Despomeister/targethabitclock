package com.example.targethabitclock.Entity;

public class ToDo {

    private String id, title, deadline, priority, idLongTerm, isDoneClickTime;
    private boolean isDone;
    private long period;

    public ToDo() {}

    public ToDo(String id, String title, String deadline, String priority, String idLongTerm, boolean isDone, String isDoneClickTime, long period) {
        this.id = id;
        this.title = title;
        this.deadline = deadline;
        this.priority = priority;
        this.idLongTerm = idLongTerm;
        this.isDone = isDone;
        this.isDoneClickTime = isDoneClickTime;
        this.period = period;
    }

    public long getPeriod() {
        return period;
    }

    public String getIsDoneClickTime() {
        return isDoneClickTime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getPriority() {
        return priority;
    }

    public String getIdLongTerm() {
        return idLongTerm;
    }

    public boolean isDone() {
        return isDone;
    }
}
