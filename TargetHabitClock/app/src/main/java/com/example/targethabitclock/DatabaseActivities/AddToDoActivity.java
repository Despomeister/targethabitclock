package com.example.targethabitclock.DatabaseActivities;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProviders;

import com.example.targethabitclock.Adapters.MySpinnerAdapter;
import com.example.targethabitclock.Entity.ToDo;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.LongTerm.ViewModel.TermViewModel;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.LongTermPreviewActivity.LongTermViewActivity;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class AddToDoActivity extends AppCompatActivity  {

    TextView toolbar_title, tv_toInv, tv_toInv2, tv_period;
    TermViewModel termViewModel;
    FirebaseUser user;
    DatabaseReference database;
    List<String> titles = new ArrayList<>();
    List<LongTermModel> termViewModels = new ArrayList<>();
    Spinner spinnerLongTerm, spinnerPriority, spinnerPeriod;
    ArrayAdapter<String> arrayAdapter;
    ArrayAdapter<CharSequence> arrayPeriodAdapter;
    ArrayAdapter<CharSequence> adapter;
    Button add,cancel, mDataPicker;
    DatePickerDialog.OnDateSetListener dateSetListener;
    long millis;
    ImageView information;
    EditText etTitle, etPeriod;
    String Uid, SetLongTerm,idLongTerm;
    LongTermModel longTermModel;
    ToDoModel toDoModel;
    int idSetLongTerm, mPeriod;
    boolean EditFlagLT, isEmpty;


    @SuppressLint({"NewApi", "ResourceAsColor", "Assert", "SetTextI18n"})
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);
        findViews();
        toolbar_title.setText("Adding new task");

        termViewModel = ViewModelProviders.of(this).get(TermViewModel.class);
        termViewModel.init(this);

        titles.add("Select Long Term: ");
        termViewModels.addAll(Objects.requireNonNull(termViewModel.getTerms().getValue()));
        termViewModels.forEach(o -> titles.add(o.getTitle()));
        arrayAdapter = new ArrayAdapter<>(this, R.layout.todo_spinner_item, Objects.requireNonNull(titles));

        adapter = ArrayAdapter.createFromResource(this, R.array.priority_array, R.layout.todo_spinner_item);

        arrayPeriodAdapter = ArrayAdapter.createFromResource(this, R.array.period_array, R.layout.todo_spinner_item);

        Bundle b = getIntent().getExtras();
        if(b != null)
        {
            if(b.getBoolean("LongTermView"))
            {
                EditFlagLT = true;
                Intent intent = getIntent();
                toDoModel =  (ToDoModel) intent.getSerializableExtra("Object");
                longTermModel = (LongTermModel) intent.getSerializableExtra("ObjectLT");
                add.setText("Edit");
                toolbar_title.setText("Editing task");
            }
            else if(b.getBoolean("ToDoEdit"))
            {
                Intent intent = getIntent();
                toDoModel =  (ToDoModel) intent.getSerializableExtra("Object");
                add.setText("Edit");
                toolbar_title.setText("Editing task");
            }
            else if(b.getBoolean("AddToDoFromLongTerm"))
            {
                isEmpty = true;
                Intent intent = getIntent();
                longTermModel = (LongTermModel) intent.getSerializableExtra("ObjectLongTerm");
            }
            else
            {
                EditFlagLT = false;
            }
        }

        setListeners();

        user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("ToDo");
        if(toDoModel == null)
        {
            Uid = database.push().getKey();
            assert Uid != null : "database-null";

            if(isEmpty)
            {
                titles.forEach(o ->{
                    if(o.equals(longTermModel.getTitle()))
                    {
                        SetLongTerm = o;
                    }
                });

                idSetLongTerm = termViewModels.indexOf(termViewModels.stream().filter(t -> t.getTitle().equals(SetLongTerm)).collect(Collectors.toList()).get(0));
                spinnerLongTerm.setSelection(idSetLongTerm +1);
                spinnerLongTerm.setEnabled(false);
                spinnerLongTerm.setVisibility(View.INVISIBLE);
                tv_toInv.setVisibility(View.INVISIBLE);
                tv_toInv2.setVisibility(View.INVISIBLE);
            }
        }
        else
        {
            Uid = toDoModel.getId();
        }

        if(toDoModel != null) {

                etTitle.setText(toDoModel.getTitle());
                if (toDoModel.getDeadline() != null) {
                    long currentDateTime = Long.parseLong(toDoModel.getDeadline());
                    Date currentDate = new Date(currentDateTime);
                    @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
                    mDataPicker.setText(df.format(currentDate));
                    millis = currentDateTime;
                }
                if (!toDoModel.getPriority().equals("null"))
                    spinnerPriority.setSelection(toDoModel.getPriority().charAt(0) - 64);


                if (toDoModel.getIdLongTerm() != null) {
                    int id = termViewModels.indexOf(termViewModels.stream().filter(o -> o.getId().equals(toDoModel.getIdLongTerm())).collect(Collectors.toList()).get(0));
                    spinnerLongTerm.setSelection(id + 1);
                }

                if(toDoModel.getPeriod() != 0)
                {
                    if(toDoModel.getPeriod() == 1)
                    spinnerPeriod.setSelection(2);
                    else if(toDoModel.getPeriod() == 7)
                    spinnerPeriod.setSelection(4);
                    else if(toDoModel.getPeriod() == 30)
                    spinnerPeriod.setSelection(5);
                    else
                    {
                        spinnerPeriod.setSelection(3);
                        etPeriod.setText("" + toDoModel.getPeriod());
                    }
                }
                else
                {
                    spinnerPeriod.setSelection(1);
                }

        }


    }

    @Override
    public void onBackPressed() {
        ExitAlert();
    }

    public void ExitAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddToDoActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setMessage("Are you sure, that you want to back without data save?")
                .setPositiveButton("No", null)
                .setNegativeButton("Yes", (dialogInterface, i) -> finish());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("SetTextI18n")
    private void setListeners() {

        arrayAdapter.setDropDownViewResource(R.layout.todo_dropdown_view);
        spinnerLongTerm.setAdapter(arrayAdapter);

        adapter.setDropDownViewResource(R.layout.todo_dropdown_view);
        spinnerPriority.setAdapter(
                new MySpinnerAdapter(
                        adapter,
                        R.layout.contact_spinner_row_nothing_selected,
                        this));

        arrayPeriodAdapter.setDropDownViewResource(R.layout.todo_dropdown_view);
        spinnerPeriod.setAdapter(
                new MySpinnerAdapter(
                        arrayPeriodAdapter,
                        R.layout.spinner_period_nothing_selected,
                        this));


        spinnerPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(spinnerPeriod.getSelectedItem() != null && spinnerPeriod.getSelectedItem().equals("Every few days"))
                {
                    etPeriod.setVisibility(View.VISIBLE);
                    tv_period.setVisibility(View.VISIBLE);
                }
                else
                {
                    tv_period.setVisibility(View.INVISIBLE);
                    etPeriod.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        mDataPicker.setOnClickListener(view -> {

            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddToDoActivity.this,android.R.style.Theme_Holo_Dialog_MinWidth,dateSetListener,year,month,day);
            Objects.requireNonNull(datePickerDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            datePickerDialog.show();
        });


        dateSetListener = (datePicker, year, month, day) -> {
            month = month+1;
            String date = day + "/" + month + "/" + year;
            mDataPicker.setText(date);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date dateIn = sdf.parse(date);
                assert dateIn != null;
                millis = dateIn.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        };


        add.setOnClickListener(view -> {
            String title = etTitle.getText().toString();
            if(title.matches(""))
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddToDoActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Task-title cannot be empty")
                        .setPositiveButton("Ok",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else
            {
                String priority = "" + spinnerPriority.getSelectedItem();
                String period = "" + spinnerPeriod.getSelectedItem();
                String periodX = etPeriod.getText().toString();


                switch (period) {
                    case "Every few days":
                        mPeriod = Integer.parseInt(periodX);
                        break;
                    case "Every day":
                        mPeriod = 1;
                        break;
                    case "One-time":
                        mPeriod = 0;
                        break;
                    case "Every week":
                        mPeriod = 7;
                        break;
                    case "Every month":
                        mPeriod = 30;
                        break;
                }


                String stringLongTerm = "" + spinnerLongTerm.getSelectedItem();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    termViewModels.forEach(o -> {
                        if(o.getTitle().equals(stringLongTerm))
                        {
                            idLongTerm = "" + o.getId();
                        }
                    });
                }

                String deadline = "" + millis;

                if(stringLongTerm.equals("Choose Long Term: "))
                {
                    if(deadline.equals("0"))
                    database.child(Uid).setValue(new ToDo(Uid,title,null,priority,null, false,"null",mPeriod));
                    else
                        database.child(Uid).setValue(new ToDo(Uid,title,deadline,priority,null, false,"null",mPeriod));

                }
                else
                {
                    if(deadline.equals("0"))
                    database.child(Uid).setValue(new ToDo(Uid,title,null,priority,idLongTerm, false,"null",mPeriod));
                    else
                        database.child(Uid).setValue(new ToDo(Uid,title,deadline,priority,idLongTerm, false,"null",mPeriod));
                }

                if(EditFlagLT)
                {
                    Intent intent = new Intent(AddToDoActivity.this, LongTermViewActivity.class);
                    intent.putExtra("LongTermEditedItemStart",true);
                    intent.putExtra("ObjectEdited",longTermModel);
                    startActivity(intent);
                }
                else if(isEmpty)
                {
                    Intent intent = new Intent(AddToDoActivity.this, LongTermViewActivity.class);
                    intent.putExtra("LongTermItemStart",true);
                    intent.putExtra("Object",longTermModel);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(AddToDoActivity.this, MainActivity.class);
                    intent.putExtra("ToDoStart",true);
                    startActivity(intent);
                }
            }
        });

        cancel.setOnClickListener(view -> ExitAlert());

        information.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(AddToDoActivity.this,  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Description of the priorities: ");
            final TextView textView = new TextView(AddToDoActivity.this);
            textView.setText("- A - Things that MUST be done, \n" +
                    "- B - Things that should be done, \n" +
                    "- C - Things that give you pleasure, \n" +
                    "- D - Things that you should delegate, \n" +
                    "- E - Things that you have to eliminate.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });
    }

    private void findViews() {

        toolbar_title = findViewById(R.id.view_toolbar_title);
        spinnerLongTerm = findViewById(R.id.spinnerWithLongTerm);
        spinnerPriority = findViewById(R.id.spinnerPriority);
        spinnerPeriod = findViewById(R.id.spinnerWithPeriod);
        add = findViewById(R.id.add_button_todo);
        cancel = findViewById(R.id.cancel_button_todo);
        mDataPicker = findViewById(R.id.buttonToDoDate);
        etTitle = findViewById(R.id.etTittleToDo);
        etPeriod = findViewById(R.id.etXPeriod);
        tv_toInv = findViewById(R.id.textView6);
        tv_toInv2 = findViewById(R.id.textView4);
        tv_period = findViewById(R.id.textView8);
        information = findViewById(R.id.informationIMG_ToDo);
    }
}
