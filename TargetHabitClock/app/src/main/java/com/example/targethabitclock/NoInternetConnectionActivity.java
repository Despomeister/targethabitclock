package com.example.targethabitclock;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.targethabitclock.Splash.SplashScreen;

public class NoInternetConnectionActivity extends AppCompatActivity {

    Button exit_btn, retry_btn;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet_connection);
        findViews();
        setListeners();
    }

    private void setListeners() {

        exit_btn.setOnClickListener(view -> finish());

        retry_btn.setOnClickListener(view -> {
            Intent intent = new Intent(this, SplashScreen.class);
            startActivity(intent);
        });
    }

    private void findViews() {
        exit_btn = findViewById(R.id.exit_button);
        retry_btn = findViewById(R.id.retry_button);
    }
}
