package com.example.targethabitclock.Fragments.Clock;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.AlarmStuff.AlarmReceiver;
import com.example.targethabitclock.AlarmStuff.SecondAlarmReceiver;
import com.example.targethabitclock.AlarmStuff.SecondRingtonePlayServices;
import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.Fragments.Clock.Adapters.RecyclerViewMorningRitualAdapter;
import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDataListener;
import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDoneDataListener;
import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;

import net.steamcrafted.lineartimepicker.dialog.LinearTimePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

@RequiresApi(api = Build.VERSION_CODES.M)
public class ClockHomeFragment extends BaseFragment {

    private Button button;
    private AlarmManager alarmManager;
    private Switch aSwitch;
    private static boolean isOn = true;
    private static int mHour = 12, mMinutes = 0;
    private static PendingIntent pendingIntent1, pendingIntent2;
    private ClockHomeFragment inst;
    private Context context;
    private ImageView information, information2;

    public ClockHomeFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clock_home, container, false);
        findViews(view);
        this.context = getContext();

//        aSwitch.setEnabled(false);

        aSwitch.setChecked(!isOn);
        String buttonText = ((mHour < 10) ? "0" + mHour : mHour + "");
        buttonText += ":";
        buttonText += ((mMinutes < 10) ? "0" + mMinutes : mMinutes + "");
        button.setText(buttonText);

        setListeners();


        return view;
    }

    @SuppressLint("SetTextI18n")
    private void setListeners() {
        if(mHour != 0 && mMinutes != 0)
        button.setText(mHour + ":" + mMinutes);

        button.setOnClickListener(view -> {
            LinearTimePickerDialog dialog = LinearTimePickerDialog.Builder.with(getContext())
                    .setDialogBackgroundColor(Objects.requireNonNull(getContext()).getColor(R.color.background))
                    .setPickerBackgroundColor(Objects.requireNonNull(getContext()).getColor(R.color.background))
                    .setLineColor(Objects.requireNonNull(getContext()).getColor(R.color.accent))
                    .setTextColor(Objects.requireNonNull(getContext()).getColor(R.color.cyan))
                    .setShowTutorial(false)
                    .setTextBackgroundColor(Objects.requireNonNull(getContext()).getColor(R.color.accent))
                    .setButtonColor(Objects.requireNonNull(getContext()).getColor(R.color.accent))
                    .setButtonCallback(new LinearTimePickerDialog.ButtonCallback() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onPositive(DialogInterface dialog, int hour, int minutes) {
                            if(minutes < 10)
                            {
                                button.setText("" + hour + ":0" + minutes);
                            }
                            else
                            {
                                button.setText("" + hour + ":" + minutes);
                            }
                            mHour = hour;
                            mMinutes = minutes;
                        }

                        @Override
                        public void onNegative(DialogInterface dialog) {
                        }
                    }).build();

            dialog.show();
        });

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        System.out.println(cal.get(Calendar.HOUR_OF_DAY));

        aSwitch.setOnClickListener(view ->
        {
            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();

            Intent intent1 = new Intent(getContext(), AlarmReceiver.class);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            Intent intent2 = new Intent(getContext(), SecondAlarmReceiver.class);
            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            isOn = !aSwitch.isChecked();

            if(aSwitch.isChecked())
            {
                if (mMinutes < 10) {
                    Toast.makeText(getContext(), "ClockAlarm is set to: " + mHour + ":0" + mMinutes, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getContext(), "ClockAlarm is set to: " + mHour + ":" + mMinutes, Toast.LENGTH_LONG).show();
                }

                if((cal.get(Calendar.HOUR_OF_DAY) > mHour) || (cal.get(Calendar.HOUR_OF_DAY) == mHour && cal.get(Calendar.MINUTE) >= mMinutes))
                {
                    calendar1.setTime(date);
                    calendar2.setTime(date);
                    calendar1.add(Calendar.DATE, 1);
                    calendar2.add(Calendar.DATE, 1);
                }


                if (mMinutes - 20 < 0) {
                    calendar1.set(Calendar.HOUR_OF_DAY, mHour - 1);
                    calendar1.set(Calendar.MINUTE, mMinutes + 40);
                    calendar2.set(Calendar.HOUR_OF_DAY,mMinutes);
                    calendar2.set(Calendar.MINUTE, mMinutes);



                    intent1.putExtra("extra", "on");
                    intent2.putExtra("extra", "on");
                    pendingIntent1 = PendingIntent.getBroadcast(getContext(), 0, intent1, PendingIntent.FLAG_ONE_SHOT);
                    pendingIntent2 = PendingIntent.getBroadcast(getContext(), 1, intent2, PendingIntent.FLAG_ONE_SHOT);
                    alarmManager = (AlarmManager) Objects.requireNonNull(getContext()).getSystemService(Context.ALARM_SERVICE);
                    assert alarmManager != null : "alarm manager null";
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), pendingIntent1);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar2.getTimeInMillis(), pendingIntent2);
                } else {
                    calendar1.set(Calendar.HOUR_OF_DAY, mHour);
                    calendar1.set(Calendar.MINUTE, mMinutes - 20

                    );
                    calendar2.set(Calendar.HOUR_OF_DAY, mHour);
                    calendar2.set(Calendar.MINUTE, mMinutes);
                    intent1.putExtra("extra", "on");
                    intent2.putExtra("extra", "on");
                    pendingIntent1 = PendingIntent.getBroadcast(getContext(), 0, intent1, PendingIntent.FLAG_ONE_SHOT);
                    pendingIntent2 = PendingIntent.getBroadcast(getContext(), 1, intent2, PendingIntent.FLAG_ONE_SHOT);
                    alarmManager = (AlarmManager) Objects.requireNonNull(getContext()).getSystemService(Context.ALARM_SERVICE);
                    assert alarmManager != null : "alarm manager null";
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), pendingIntent1);
                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar2.getTimeInMillis(), pendingIntent2);
                }
            }
            else
            {
                if (alarmManager == null)
                    alarmManager = (AlarmManager) Objects.requireNonNull(getContext()).getSystemService(Context.ALARM_SERVICE);
                Toast.makeText(getContext(),"ClockAlarm is unset", Toast.LENGTH_LONG).show();

                intent1.putExtra("extra", "off");
                Objects.requireNonNull(getContext()).sendBroadcast(intent1);
                alarmManager.cancel(pendingIntent1);

                intent2.putExtra("extra", "off");
                getContext().sendBroadcast(intent2);
                alarmManager.cancel(pendingIntent2);
            }

        });

        if(cal.get(Calendar.HOUR_OF_DAY) == mHour && cal.get(Calendar.MINUTE) == mMinutes+20)
        {
            aSwitch.setChecked(false);
        }

        information.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext(),  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("How does the clock work?");
            final TextView textView = new TextView(getContext());
            textView.setText("The alarm clock has two phases, i.e. it consists of two alarms after each other.\n" +
                    "The first is a quiet sound in the form of, for example, the sound of waves or trees,\n" +
                    "aimed at to wake us up from deep sleep, but not to wake up completely.\n" +
                    "Then after a few minutes, the default sound will start, which is to wake us up from sleep.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });

        information2.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext(),  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Example of falling asleep technique: ");
            final TextView textView = new TextView(getContext());
            textView.setText("4-7-8 method: \n" +
                    "First you have to sit in a comfortable position on the bed." +
                    "Then just breathe in through your nose for four seconds,hold your breath for seven seconds," +
                    "and then slowly exhale through your mouth for another eight seconds. Repeat until you fall asleep.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });
    }

    private void findViews(View view) {

        button = view.findViewById(R.id.button);
        aSwitch = view.findViewById(R.id.switch1);
        information = view.findViewById(R.id.toolbar_information);
        information2 = view.findViewById(R.id.toolbar_information2);
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }
}
