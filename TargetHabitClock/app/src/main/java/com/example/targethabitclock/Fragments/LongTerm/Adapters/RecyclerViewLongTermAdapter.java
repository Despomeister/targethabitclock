package com.example.targethabitclock.Fragments.LongTerm.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.R;
import java.util.ArrayList;
import java.util.Date;

public class RecyclerViewLongTermAdapter extends RecyclerView.Adapter<RecyclerViewLongTermAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<LongTermModel> termModels;
    private OnItemClickListener listener;
    private boolean progress;
    private long termDeadline;


    public RecyclerViewLongTermAdapter(ArrayList<LongTermModel> termModels,Context context, OnItemClickListener onItemClickListener, boolean progress) {
        this.termModels = termModels;
        this.mContext = context;
        this.listener = onItemClickListener;
        this.progress = progress;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.longterm_item_view,parent,false);

        return new ViewHolder(view);
    }

    @SuppressLint({"SetTextI18n", "ResourceAsColor", "NewApi"})
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

            final LongTermModel objIncome = termModels.get(position);
            holder.itemView.setTag(termModels.get(position).getId());
            holder.term.setText(termModels.get(position).getTitle());
            holder.term.setTextColor(this.mContext.getColor(R.color.best));

            Date date = new Date();
            if(!termModels.get(position).getDeadline().equals("Deadline unset."))
                termDeadline = Long.parseLong(termModels.get(position).getDeadline());

            if(termDeadline < date.getTime() && !termModels.get(position).isDone())
            {
                holder.term.setTextColor(this.mContext.getColor(R.color.warning));
            }
            else
            {
                switch (termModels.get(position).getPriority()) {
                    case "A":
                    case "B":
                    case "C":
                        break;
                    case "D":
                        holder.term.setTextColor(this.mContext.getColor(R.color.accent));
                        break;
                    case "E":
                        holder.term.setTextColor(this.mContext.getColor(R.color.warning2));
                        break;
                    default:
                        if(!termModels.get(position).isDone())
                        holder.term.setTextColor(this.mContext.getColor(R.color.warning));
                }
            }

            if(progress)
            holder.progress.setVisibility(View.VISIBLE);
            else
            holder.progress.setVisibility(View.INVISIBLE);

            holder.itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.OnTermClick(position, objIncome);
                }
            });

    }


    @Override
    public int getItemCount() {
        return termModels.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder{

        TextView term;
        ImageView progress;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            term = itemView.findViewById(R.id.longterm_term);
            progress = itemView.findViewById(R.id.longterm_done);
        }
    }

    public interface OnItemClickListener {
        void OnTermClick(int position, LongTermModel object);
    }
}
