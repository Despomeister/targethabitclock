package com.example.targethabitclock.Fragments.Clock.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.R;

import java.util.ArrayList;

public class RecyclerViewMorningRitualAdapter extends RecyclerView.Adapter<RecyclerViewMorningRitualAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MorningRitualModel> morningRitualModels;
    private OnItemClickListener listener;

    public RecyclerViewMorningRitualAdapter(Context mContext, ArrayList<MorningRitualModel> morningRitualModels, OnItemClickListener listener) {
        this.mContext = mContext;
        this.morningRitualModels = morningRitualModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.morning_ritual_item_view,parent,false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final MorningRitualModel objIncome = morningRitualModels.get(position);
        holder.itemView.setTag(morningRitualModels.get(position).getId());
        holder.task.setText(morningRitualModels.get(position).getTitle());
        holder.task.setTextColor(this.mContext.getColor(R.color.primary_light));

        if (listener != null) {
            listener.OnMorningTaskClick(position, objIncome,holder.checkBox,holder.itemView,holder.bucket);
        }
    }

    @Override
    public int getItemCount() {
        return morningRitualModels.size();
    }



    static class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox checkBox;
        TextView task;
        ImageView bucket;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBox= itemView.findViewById(R.id.MorningRitualCheckBox);
            task = itemView.findViewById(R.id.MorningRitualTask);
            bucket = itemView.findViewById(R.id.bucket);
        }
    }

    public interface OnItemClickListener {
        void OnMorningTaskClick(int position, MorningRitualModel object, CheckBox checkBox,View itemView, ImageView imageView);
    }

}
