package com.example.targethabitclock.Entity;

public class LongTerm {

    private String id, title, description, deadline, priority;
    private boolean isDone;



    public LongTerm() {}

    public LongTerm(String id, String title, String description, String deadline, String priority, boolean isDone) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.deadline = deadline;
        this.priority = priority;
        this.isDone = isDone;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getPriority() {
        return priority;
    }

    public boolean isDone() {
        return isDone;
    }
}
