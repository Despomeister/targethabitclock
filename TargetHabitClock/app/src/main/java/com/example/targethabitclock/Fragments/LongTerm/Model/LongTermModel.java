package com.example.targethabitclock.Fragments.LongTerm.Model;

import java.io.Serializable;

public class LongTermModel implements Serializable {

    private String deadline, description, id, priority, title;

    private boolean done;

    public void setDone(boolean done) {
        this.done = done;
    }

    public LongTermModel()
    {}

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public String getPriority() {
        return priority;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

}
