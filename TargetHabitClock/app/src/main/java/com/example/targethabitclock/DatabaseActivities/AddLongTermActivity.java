package com.example.targethabitclock.DatabaseActivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.example.targethabitclock.Adapters.MySpinnerAdapter;
import com.example.targethabitclock.Entity.LongTerm;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class AddLongTermActivity extends AppCompatActivity {

    Spinner spinner;
    Button add,cancel,mDataPicker;
    AutoCompleteTextView etTitle, etDescription;
    DatabaseReference database;
    DatePickerDialog.OnDateSetListener dateSetListener;
    FirebaseUser user;
    long millis;
    TextView toolbar;
    ImageView information, information2;


    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_longterm);
        findViews();
        setListeners();

        user = FirebaseAuth.getInstance().getCurrentUser();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.priority_array, R.layout.todo_spinner_item);
        adapter.setDropDownViewResource(R.layout.todo_dropdown_view);

        spinner.setAdapter(
                new MySpinnerAdapter(
                        adapter,
                        R.layout.contact_spinner_row_nothing_selected,
                        this));

    }

    @Override
    public void onBackPressed() {
        ExitAlert();
    }

    public void ExitAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddLongTermActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setMessage("Are you sure, that you want to back without data save?")
                .setPositiveButton("No", null)
                .setNegativeButton("Yes", (dialogInterface, i) -> finish());

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setListeners() {

        toolbar.setText("Adding new Long Term");

        add.setOnClickListener(view -> {
            String title = etTitle.getText().toString();
            String description = etDescription.getText().toString();
            if(title.matches(""))
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddLongTermActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Term-title cannot be empty")
                        .setPositiveButton("Ok",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else
            {
                System.out.println("///////////////////////////////////");
                System.out.println(millis);
                String deadline;
                if(millis == 0)
                {
                    deadline = "Deadline unset.";
                }
                else
                {
                    deadline = "" + millis;
                }
                System.out.println("///////////////////////////////////");

                String priority = "" + spinner.getSelectedItem();
                database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("LongTerm");
                String id = database.push().getKey();
                assert id != null : "database-null";
                database.child(id).setValue(new LongTerm(id,title,description,deadline,"" + priority, false));

                Intent intent = new Intent(AddLongTermActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        cancel.setOnClickListener(view -> ExitAlert());

        mDataPicker.setOnClickListener(view -> {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(AddLongTermActivity.this,android.R.style.Theme_Holo_Dialog_MinWidth,dateSetListener,year,month,day);
            Objects.requireNonNull(datePickerDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            datePickerDialog.show();
        });

        dateSetListener = (datePicker, year, month, day) -> {
            month = month+1;
            String date = day + "/" + month + "/" + year;
            mDataPicker.setText(date);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date dateIn = sdf.parse(date);
                assert dateIn != null;
                millis = dateIn.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        };

        information.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(AddLongTermActivity.this,  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Description of the priorities: ");
            final TextView textView = new TextView(AddLongTermActivity.this);
            textView.setText("- A - Things that MUST be done, \n" +
                    "- B - Things that should be done, \n" +
                    "- C - Things that give you pleasure, \n" +
                    "- D - Things that you should delegate, \n" +
                    "- E - Things that you have to eliminate.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });

        information2.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(AddLongTermActivity.this,  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Description of the correct long term: ");
            final TextView textView = new TextView(AddLongTermActivity.this);
            textView.setText("- Specific – target a specific area for improvement. \n" +
                    "- Measurable – quantify or at least suggest an indicator of progress.\n"  +
                    "- Assignable – specify who will do it.\n"  +
                    "- Realistic – state what results can realistically be achieved, given available resources.\n"  +
                    "- Time-related – specify when the result(s) can be achieved.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });
    }

    private void findViews() {
        add = findViewById(R.id.add_button_longterm);
        cancel = findViewById(R.id.cancel_button_longterm);
        spinner =  findViewById(R.id.spinner);
        etTitle = findViewById(R.id.etTitleLongTerm);
        etDescription = findViewById(R.id.etDescriptionLongTerm);
        mDataPicker = findViewById(R.id.buttonDate);
        toolbar = findViewById(R.id.view_toolbar_title);
        information = findViewById(R.id.informationIMG_LongTerm);
        information2 = findViewById(R.id.informationIMG_LongTermDescription);
    }

}