package com.example.targethabitclock.Fragments.Clock;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.DatabaseActivities.AddLongTermActivity;
import com.example.targethabitclock.DatabaseActivities.AddMorningRitualActivity;
import com.example.targethabitclock.Fragments.Clock.Adapters.RecyclerViewMorningRitualAdapter;
import com.example.targethabitclock.Fragments.Clock.Adapters.RecyclerViewMorningRitualDoneAdapter;
import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDataListener;
import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDoneDataListener;
import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.Fragments.Clock.ViewModel.MorningRitualDoneViewModel;
import com.example.targethabitclock.Fragments.Clock.ViewModel.MorningRitualViewModel;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class ClockMorningRitualFragment extends BaseFragment implements ClockMorningRitualDataListener, RecyclerViewMorningRitualAdapter.OnItemClickListener, ClockMorningRitualDoneDataListener, RecyclerViewMorningRitualDoneAdapter.OnItemClickListener {

    private RecyclerView recyclerView, recyclerView2;
    private RecyclerViewMorningRitualAdapter recyclerViewMorningRitualAdapter;
    private RecyclerViewMorningRitualDoneAdapter recyclerViewMorningRitualDoneAdapter;
    private DatabaseReference database;
    private ArrayList<MorningRitualModel> morningRitualModels;
    private ArrayList<MorningRitualModel> morningRitualDoneModels;
    private MorningRitualViewModel morningRitualModel;
    private MorningRitualDoneViewModel morningRitualDoneViewModel;
    private ImageView information;

    public ClockMorningRitualFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_clock_morning_ritual, container, false);
        findViews(view);
        getData();

        setListeners();
        return view;
    }

    @SuppressLint("SetTextI18n")
    private void setListeners() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewMorningRitualAdapter);

        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView2.setAdapter(recyclerViewMorningRitualDoneAdapter);

        information.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext(),  AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("What exactly is morning ritual?");
            final TextView textView = new TextView(getContext());
            textView.setText("Morning ritual is a set of regularly repeated activities." +
                    " Consistent performance of properly selected morning" +
                    " activities can bring measurable effects:" +
                    " it can help maintain internal peace, provide energy for the" +
                    " whole day and achieve the desired work life balance. \n" +
                    "Examples: \n" +
                    "- cold shower, \n" +
                    "- meditation, \n" +
                    "- reading a book.");
            textView.setPadding(40,10,20,10);
            textView.setTextColor(getResources().getColor(R.color.primary_light));
            dialog.setView(textView);
            dialog.setPositiveButton("Ok", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });

    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.MorningRitualRecyclerView);
        recyclerView2 = view.findViewById(R.id.MorningRitualRecyclerView2);
        information = view.findViewById(R.id.toolbar_information);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getData() {
        morningRitualModel = ViewModelProviders.of(ClockMorningRitualFragment.this).get(MorningRitualViewModel.class);
        morningRitualModel.init(ClockMorningRitualFragment.this);
        recyclerViewMorningRitualAdapter = new RecyclerViewMorningRitualAdapter(getContext(),morningRitualModel.getMorningRitual().getValue(), this);
        morningRitualModels = morningRitualModel.getMorningRitual().getValue();

        morningRitualDoneViewModel = ViewModelProviders.of(ClockMorningRitualFragment.this).get(MorningRitualDoneViewModel.class);
        morningRitualDoneViewModel.init(ClockMorningRitualFragment.this);
        recyclerViewMorningRitualDoneAdapter = new RecyclerViewMorningRitualDoneAdapter(getContext(), morningRitualDoneViewModel.getMorningRitualDone().getValue(), this);
        morningRitualDoneModels = morningRitualDoneViewModel.getMorningRitualDone().getValue();
    }


    @Override
    public void onClockMorningRitualLoaded() {
        morningRitualModel.getMorningRitual().observe(this,morningRitualModels -> recyclerViewMorningRitualAdapter.notifyDataSetChanged());
    }

    @Override
    public void onClockMorningRitualDoneLoaded() {
        morningRitualDoneViewModel.getMorningRitualDone().observe(this,morningRitualModels -> recyclerViewMorningRitualDoneAdapter.notifyDataSetChanged());
    }


    @Override
    public void OnMorningTaskClick(int position, MorningRitualModel object, CheckBox checkBox, View itemView, ImageView imageView) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("MorningRitual");
        if(!morningRitualModels.get(position).isDone())
        {
            checkBox.setOnClickListener(view -> {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Have you really done this task?")
                        .setPositiveButton("No", (dialogInterface, i) -> checkBox.setChecked(false))
                        .setNegativeButton("Yes", (dialogInterface, i) -> {


                            Date date = new Date();
                            long timeMilli = date.getTime();
                            String isDoneTime = "" + timeMilli;
                            morningRitualModels.get(position).setDone(true);
                            morningRitualModels.get(position).setIsDoneClickTime(isDoneTime);
                            database.child(morningRitualModels.get(position).getId()).setValue(morningRitualModels.get(position));

                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("MorningRitualStart",true);
                            startActivity(intent);

                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            });


            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(getContext(), AddMorningRitualActivity.class);
                intent.putExtra("Object", object);
                intent.putExtra("MorningRitualEdit",true);
                startActivity(intent);
            });

            imageView.setOnClickListener(view -> {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("You really want to delete this task permanently?")
                        .setPositiveButton("No", null)
                        .setNegativeButton("Yes", (dialogInterface, i) -> {

                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("MorningRitual").child(morningRitualModels.get(position).getId());
                            databaseReference.removeValue();

                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("MorningRitualStart",true);
                            startActivity(intent);

                        });
                AlertDialog dialog = builder.create();
                dialog.show();


            });
        }
    }
    @Override
    public void OnMorningTaskDoneClick(int position, MorningRitualModel object) {

    }
}