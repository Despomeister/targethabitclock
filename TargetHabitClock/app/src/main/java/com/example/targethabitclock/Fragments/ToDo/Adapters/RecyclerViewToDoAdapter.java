package com.example.targethabitclock.Fragments.ToDo.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.R;

import java.util.ArrayList;
import java.util.Date;


public class RecyclerViewToDoAdapter extends RecyclerView.Adapter<RecyclerViewToDoAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<ToDoModel> toDoModels;
    private OnItemClickListener listener;

    public RecyclerViewToDoAdapter(Context mContext, ArrayList<ToDoModel> toDoModels, OnItemClickListener listener) {
        this.mContext = mContext;
        this.toDoModels = toDoModels;
        this.listener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todo_item_view,parent,false);

        return new RecyclerViewToDoAdapter.ViewHolder(view);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ToDoModel objIncome = toDoModels.get(position);
        holder.itemView.setTag(toDoModels.get(position).getId());
        holder.task.setText(toDoModels.get(position).getTitle());
        holder.task.setTextColor(this.mContext.getColor(R.color.best));

        Date date = new Date();
        long termDeadline;
        if(toDoModels.get(position).getDeadline() != null)
        {
            termDeadline = Long.parseLong(toDoModels.get(position).getDeadline());
        }
        else
        {
            termDeadline = 0;
        }


        if(termDeadline < date.getTime() && !toDoModels.get(position).isDone())
        {
            holder.task.setTextColor(this.mContext.getColor(R.color.warning));
        }
        else
        {
            switch (toDoModels.get(position).getPriority()) {
                case "A":
                case "B":
                case "C":
                    break;
                case "D":
                    holder.task.setTextColor(this.mContext.getColor(R.color.accent));
                    break;
                case "E":
                    holder.task.setTextColor(this.mContext.getColor(R.color.warning2));
                    break;
                    default:
                        holder.task.setTextColor(this.mContext.getColor(R.color.warning));
            }
        }

        if (listener != null) {
            listener.OnToDoClick(position, objIncome, holder.checkBox,holder.itemView);
        }
    }

    @Override
    public int getItemCount() {
        return toDoModels.size();
    }

    public ToDoModel getToDoAt(int position)
    {
        return toDoModels.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView task;
        CheckBox checkBox;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            task = itemView.findViewById(R.id.ToDoTerm);
            checkBox = itemView.findViewById(R.id.rvToDoCheckBox);
            checkBox.setChecked(false);
        }
    }

    public interface OnItemClickListener {
        void OnToDoClick(int position, ToDoModel object, CheckBox checkBox,View itemView);
    }
}
