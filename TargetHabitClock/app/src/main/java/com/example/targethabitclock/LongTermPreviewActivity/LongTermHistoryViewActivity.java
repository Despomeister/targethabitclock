package com.example.targethabitclock.LongTermPreviewActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.ToDo.Adapters.RecyclerViewToDoDailyAdapter;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.ToDoDailySuccessFragment;
import com.example.targethabitclock.Fragments.ToDo.ViewModel.DailySuccessViewModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

import me.itangqi.waveloadingview.WaveLoadingView;

public class LongTermHistoryViewActivity extends AppCompatActivity implements ToDoDataHistoryLoadListener, RecyclerViewToDoDailyAdapter.OnItemClickListener {

    private DailySuccessViewModel dailySuccessViewModel;
    private RecyclerViewToDoDailyAdapter recyclerViewToDoDailyAdapter;
    private RecyclerView recyclerView;
    LongTermModel longTermModel;
    TextView descriptionTV, deadlineTV, toolbar_tv;
    WaveLoadingView waveLoadingView;
    Button back, delete;
    ArrayList<ToDoModel> toDoDailySuccessModels;
    private DailySuccessViewModel toDoViewModel;




    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longterm_history_view);
        findViews();
        getItems();

        dailySuccessViewModel = ViewModelProviders.of(this).get(DailySuccessViewModel.class);
        dailySuccessViewModel.initSuccess(this);

        setListener();
    }

    private void setListener() {
        back.setOnClickListener(view -> finish());
        descriptionTV.setText(longTermModel.getDescription());

        if(longTermModel.getDeadline().equals("Deadline unset."))
        {
            deadlineTV.setText(longTermModel.getDeadline());

        }
        else
        {
            long currentDateTime = Long.parseLong(longTermModel.getDeadline());
            Date currentDate = new Date(currentDateTime);
            @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.format(currentDate);
            deadlineTV.setText(df.format(currentDate));
        }

        String text = "Term: " + longTermModel.getTitle();
        toolbar_tv.setText(text);

    }

    private void getItems() {
        Bundle b = getIntent().getExtras();
        if(b != null) {
            longTermModel = (LongTermModel) b.getSerializable("Object");
        }
    }

    private void findViews() {
        back = findViewById(R.id.back_button);
        descriptionTV = findViewById(R.id.LongTermDescription);
        waveLoadingView = findViewById(R.id.waveCircleBar);
        toolbar_tv = findViewById(R.id.view_toolbar_title);
        deadlineTV = findViewById(R.id.LongTermDeadlineText);
        recyclerView = findViewById(R.id.LongTermViewToDoDoneRecyclerView);
        delete = findViewById(R.id.delete_button);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onToDoDailySuccessLoaded() {
         toDoDailySuccessModels = Objects.requireNonNull(dailySuccessViewModel.getToDoDailySuccess().getValue()).stream()
                .filter(o -> longTermModel.getId().equals(o.getIdLongTerm()))
                .collect(Collectors.toCollection(ArrayList::new));
        recyclerViewToDoDailyAdapter = new RecyclerViewToDoDailyAdapter(this, toDoDailySuccessModels,this);
        dailySuccessViewModel.getToDoDailySuccess().observe(this,toDoModel -> recyclerViewToDoDailyAdapter.notifyDataSetChanged());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewToDoDailyAdapter);


        waveLoadingView.setProgressValue(100);
        waveLoadingView.setCenterTitle("Progress: " + waveLoadingView.getProgressValue() + "%");

        if(toDoDailySuccessModels.size() == 0)
            delete.setOnClickListener(view -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(LongTermHistoryViewActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("You really want to delete this term permanently?")
                        .setPositiveButton("No", null)
                        .setNegativeButton("Yes", (dialogInterface, i) -> {

                            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                            assert user != null;
                            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("LongTerm").child(longTermModel.getId());
                            databaseReference.removeValue();

                            Intent intent = new Intent(LongTermHistoryViewActivity.this, MainActivity.class);
                            intent.putExtra("LongTermHistoryStart",true);
                            startActivity(intent);

                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void OnToDoClick(int position, ToDoModel object, CheckBox checkBox, View itemView, ImageView imageView, ImageView imageView2) {
        checkBox.setEnabled(false);
        checkBox.setChecked(true);
        imageView.setEnabled(false);
        imageView.setVisibility(View.INVISIBLE);

        delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(LongTermHistoryViewActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            builder.setMessage("You really want to delete this term permanently?")
                    .setPositiveButton("No", null)
                    .setNegativeButton("Yes", (dialogInterface, i) -> {

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        assert user != null;
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("LongTerm").child(longTermModel.getId());
                        databaseReference.removeValue();

                        toDoDailySuccessModels.forEach(o -> {
                            DatabaseReference databaseReference1 = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("ToDo").child(o.getId());
                            databaseReference1.removeValue();

                        });

                        Intent intent = new Intent(LongTermHistoryViewActivity.this, MainActivity.class);
                        intent.putExtra("LongTermHistoryStart",true);
                        startActivity(intent);

                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        });
    }
}
