package com.example.targethabitclock.Fragments.Clock.Repository;

import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDataListener;
import com.example.targethabitclock.Fragments.Clock.Interface.ClockMorningRitualDoneDataListener;
import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class MorningRitualRepository {

    private ArrayList<MorningRitualModel> morningRitualModels = new ArrayList<>();
    private ArrayList<MorningRitualModel> morningRitualDoneModels = new ArrayList<>();
    private static MorningRitualRepository instance;
    private static ClockMorningRitualDataListener clockMorningRitualDataListener;
    private static ClockMorningRitualDoneDataListener clockMorningRitualDoneDataListener;
    private long endTime;
    private DatabaseReference database;
    private Date date;

    public static MorningRitualRepository getInstance(Fragment fragment) {

        if(instance == null)
        {
            instance = new MorningRitualRepository();
        }
        if(fragment instanceof ClockMorningRitualDataListener)
        {
            clockMorningRitualDataListener = (ClockMorningRitualDataListener) fragment;
        }
        if(fragment instanceof ClockMorningRitualDoneDataListener)
        {
            clockMorningRitualDoneDataListener = (ClockMorningRitualDoneDataListener) fragment;
        }
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<MorningRitualModel>> getMorningRitual(){
        loadTerms();
        MutableLiveData<ArrayList<MorningRitualModel>> morningRitual = new MutableLiveData<>();
        morningRitual.setValue(morningRitualModels);
        return morningRitual;


    }@RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<MorningRitualModel>> getMorningRitualDone(){
        loadTerms();
        MutableLiveData<ArrayList<MorningRitualModel>> morningRitual = new MutableLiveData<>();
        morningRitual.setValue(morningRitualDoneModels);
        return morningRitual;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadTerms() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        Query query = reference.child("Users").child(user.getUid()).child("MorningRitual");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                morningRitualModels.clear();
                morningRitualDoneModels.clear();
                for( DataSnapshot snapshot :  dataSnapshot.getChildren())
                {
                    date = new Date();
                    database = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("MorningRitual").child(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getId());
                    if(!Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).isDone())
                    {
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);


                        if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Monday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Tuesday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Wednesday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Thursday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Friday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Saturday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Sunday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))
                        {
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        }
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("null"))
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));
                        else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("-"))
                            morningRitualModels.add(snapshot.getValue(MorningRitualModel.class));

                    }
                    else
                    {
                        endTime = Long.parseLong(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getIsDoneClickTime());
                        endTime = (long) Math.floor(endTime / 86400000);

                        if ((endTime * 86400000) < date.getTime() && date.getTime() < ((endTime + 1) * 86400000))
                        {
                            morningRitualDoneModels.add(snapshot.getValue(MorningRitualModel.class));
                        }

                        if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("null"))
                        {
                            SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            System.out.println("jestem");
                        }
                        else
                        {
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);

                            if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Monday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Tuesday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Wednesday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Thursday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Friday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Saturday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                            else if(Objects.requireNonNull(snapshot.getValue(MorningRitualModel.class)).getDayOfWeek().equals("Sunday") && (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY))
                            {
                                SetCyclicTasks(snapshot.getValue(MorningRitualModel.class));
                            }
                        }
                    }
                }
                clockMorningRitualDataListener.onClockMorningRitualLoaded();
                clockMorningRitualDoneDataListener.onClockMorningRitualDoneLoaded();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        query.addListenerForSingleValueEvent(valueEventListener);
    }

    private void SetCyclicTasks(MorningRitualModel morningRitualModel)
    {
        endTime = Long.parseLong(Objects.requireNonNull(morningRitualModel).getIsDoneClickTime());
        endTime = (long) Math.floor(endTime / 86400000);
        long period = Objects.requireNonNull(morningRitualModel).getPeriod();

        if (date.getTime() >= ((endTime + period) * 86400000))
        {
            Objects.requireNonNull(morningRitualModel).setDone(false);
            Objects.requireNonNull(morningRitualModel).setIsDoneClickTime("null");
            MorningRitualModel obiekt = new MorningRitualModel(
                    Objects.requireNonNull(morningRitualModel).getId(),
                    Objects.requireNonNull(morningRitualModel).getTitle(),
                    Objects.requireNonNull(morningRitualModel).getDayOfWeek(),
                    false,
                    Objects.requireNonNull(morningRitualModel).getPeriod(),
                    "null");
            database.setValue(obiekt);
            morningRitualModels.add(obiekt);
        }
    }
}
