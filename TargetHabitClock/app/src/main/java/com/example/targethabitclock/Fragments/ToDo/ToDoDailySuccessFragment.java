package com.example.targethabitclock.Fragments.ToDo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.Fragments.ToDo.Adapters.RecyclerViewToDoDailyAdapter;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.ViewModel.DailySuccessViewModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Date;

public class ToDoDailySuccessFragment extends BaseFragment implements ToDoDataHistoryLoadListener, RecyclerViewToDoDailyAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerViewToDoDailyAdapter recyclerViewToDoAdapter;
    private DailySuccessViewModel toDoViewModel;
    private ArrayList<ToDoModel> toDoModels;

    public ToDoDailySuccessFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do_succes, container, false);
        findViews(view);
        toDoViewModel = ViewModelProviders.of(ToDoDailySuccessFragment.this).get(DailySuccessViewModel.class);
        getData();

        setListeners();
        return view;
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getData() {
        toDoViewModel.initSuccess(ToDoDailySuccessFragment.this);
        toDoModels = toDoViewModel.getToDoDailySuccessToday().getValue();
        recyclerViewToDoAdapter = new RecyclerViewToDoDailyAdapter(getContext(), toDoViewModel.getToDoDailySuccessToday().getValue(),this);
    }

    private void setListeners() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewToDoAdapter);
    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.ToDoDailyRecyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onToDoDailySuccessLoaded() {
        Date date = new Date();
        System.out.println("//////////////////////////////");
        System.out.println("Current time: " + date.getTime());
        System.out.println("//////////////////////////////");
        long endTime;
        endTime = (long) Math.floor(date.getTime() / 86400000);
        System.out.println("//////////////////////////////");
        System.out.println("Start time: " + (endTime * 86400000));
        System.out.println("//////////////////////////////");
        System.out.println("//////////////////////////////");
        System.out.println("End time: " + ((endTime+1) * 86400000));
        System.out.println("//////////////////////////////");
        toDoViewModel.getToDoDailySuccessToday().observe(this,toDoModel -> recyclerViewToDoAdapter.notifyDataSetChanged());

    }

    @Override
    public void OnToDoClick(int position, ToDoModel object, CheckBox checkBox, View itemView, ImageView imageView, ImageView imageView2) {
        checkBox.setChecked(true);
        checkBox.setEnabled(false);

        imageView.setOnClickListener(view -> {

            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            builder.setMessage("You really want to delete this task permanently?")
                    .setPositiveButton("No", null)
                    .setNegativeButton("Yes", (dialogInterface, i) -> {

                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        assert user != null;
                        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("ToDo").child(toDoModels.get(position).getId());
                        databaseReference.removeValue();

                        Intent intent = new Intent(getContext(), MainActivity.class);
                        intent.putExtra("ToDoDailyStart",true);
                        startActivity(intent);

                    });
            AlertDialog dialog = builder.create();
            dialog.show();


        });
    }


}

