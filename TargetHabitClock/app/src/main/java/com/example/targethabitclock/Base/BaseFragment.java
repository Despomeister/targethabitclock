package com.example.targethabitclock.Base;


import android.content.Context;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.targethabitclock.Enumerators.FragmentsEnumerator;
import com.example.targethabitclock.Navigation.NavigationListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BaseFragment extends Fragment {

    private NavigationListener navigationListener;
    protected FragmentsEnumerator fragmentsEnumerator ;
    protected FloatingActionButton floatingActionButton;


    public NavigationListener getNavigationsInteractions()
    {
        return navigationListener;
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        navigationListener = null;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        if (context instanceof NavigationListener)
        {
            navigationListener = (NavigationListener) context;
        }
    }

    public BaseFragment(FloatingActionButton floatingActionButton) {
        this.floatingActionButton = floatingActionButton;
    }

    public FragmentsEnumerator getEnum()
    {
        return fragmentsEnumerator;
    }
}
