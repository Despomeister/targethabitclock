package com.example.targethabitclock.Fragments.Clock.ViewModel;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.Fragments.Clock.Repository.MorningRitualRepository;

import java.util.ArrayList;

public class MorningRitualViewModel extends ViewModel {

    private MutableLiveData<ArrayList<MorningRitualModel>> morningRitualList;
    private Fragment mFragment;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Fragment fragment)
    {
        if(morningRitualList != null)
        {
            return;
        }
        mFragment = fragment;
        morningRitualList = MorningRitualRepository.getInstance(fragment).getMorningRitual();
    }

    public LiveData<ArrayList<MorningRitualModel>> getMorningRitual(){
        return morningRitualList;
    }

}
