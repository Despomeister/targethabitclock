package com.example.targethabitclock;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.FragmentManager;

import com.example.targethabitclock.DatabaseActivities.AddLongTermActivity;
import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.DatabaseActivities.AddMorningRitualActivity;
import com.example.targethabitclock.DatabaseActivities.AddToDoActivity;
import com.example.targethabitclock.Enumerators.FragmentsEnumerator;
import com.example.targethabitclock.Fragments.Clock.ClockFragment;
import com.example.targethabitclock.Fragments.LongTerm.LongTermFragment;
import com.example.targethabitclock.Fragments.ToDo.ToDoFragment;
import com.firebase.ui.auth.AuthUI;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

import static com.google.android.material.snackbar.Snackbar.LENGTH_SHORT;

public class MainActivity extends AppCompatActivity {

    CoordinatorLayout Container;
    TextView LongTermButton, ToDoButton, ClockButton, ToolbarTittle;
    BaseFragment selectedFragment = null;
    FloatingActionButton floatingActionButton;
    Toolbar toolbar;
    LinearLayout dots;
    private long mLastClickTime = 0, mEndTime = 100;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        setListeners();



        if (savedInstanceState == null)
        {
            Bundle b = getIntent().getExtras();
            if(b != null)
            {
                boolean ToDoStart = b.getBoolean("ToDoStart");
                boolean ToDoDailyStart = b.getBoolean("ToDoDailyStart");
                boolean LongTermHistoryStart = b.getBoolean("LongTermHistoryStart");
                boolean MorningRitualStart = b.getBoolean("MorningRitualStart");
                if (ToDoStart)
                {
                    selectedFragment = new ToDoFragment(floatingActionButton, dots);
                    FragmentManager fm = getSupportFragmentManager();
                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
                    setBottomButtonStyle(ToDoButton, LongTermButton, ClockButton);
                    ToolbarTittle.setText(R.string.fragmentToDoToolbar);
                }
                else if(ToDoDailyStart)
                {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("ToDoDailyStart", true);
                    selectedFragment = new ToDoFragment(floatingActionButton, dots);
                    selectedFragment.setArguments(bundle);
                    FragmentManager fm = getSupportFragmentManager();
                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
                    setBottomButtonStyle(ToDoButton, LongTermButton, ClockButton);
                    ToolbarTittle.setText(R.string.fragmentToDoToolbar);
                }
                else if(LongTermHistoryStart)
                {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("LongTermHistoryStart", true);
                    selectedFragment = new LongTermFragment(floatingActionButton,dots);
                    selectedFragment.setArguments(bundle);
                    FragmentManager fm = getSupportFragmentManager();
                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
                    setBottomButtonStyle(LongTermButton,ToDoButton,ClockButton);
                }
                else if(MorningRitualStart)
                {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("MorningRitualStart", true);
                    selectedFragment = new ClockFragment(floatingActionButton,dots,ToolbarTittle);
                    selectedFragment.setArguments(bundle);
                    FragmentManager fm = getSupportFragmentManager();
                    assert selectedFragment != null;
                    getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
                    setBottomButtonStyle(ClockButton,LongTermButton,ToDoButton);
                    ToolbarTittle.setText(R.string.fragmentMorningRitualToolbar);
                }
            }
            else
            {
                setBottomButtonStyle(LongTermButton,ToDoButton,ClockButton);
                selectedFragment = new LongTermFragment(floatingActionButton,dots);
                getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
            }
        }
    }


    private void findViews()
    {
        Container = findViewById(R.id.container);
        LongTermButton = findViewById(R.id.longtermbutton);
        ToDoButton = findViewById(R.id.todobutton);
        ClockButton = findViewById(R.id.clockbutton);
        floatingActionButton = findViewById(R.id.ftnButton);
        toolbar = findViewById(R.id.toolbar);
        ToolbarTittle = findViewById(R.id.toolbar_title);
        dots = findViewById(R.id.dots);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.logout:
                AuthUI.getInstance().signOut(Objects.requireNonNull(this)).addOnCompleteListener(task -> startActivity(new Intent(MainActivity.this, LoginActivity.class)))
                        .addOnFailureListener(e -> Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show());
                break;
            case R.id.about:
                Intent intent = new Intent(this,AboutActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void setListeners() {
        LongTermButton.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < mEndTime){
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            selectedFragment = new LongTermFragment(floatingActionButton,dots);
            FragmentManager fm = getSupportFragmentManager();
            assert selectedFragment != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
            setBottomButtonStyle(LongTermButton,ToDoButton,ClockButton);
            ToolbarTittle.setText(R.string.fragmentLongTermToolbar);
        });
        ToDoButton.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < mEndTime){
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            selectedFragment = new ToDoFragment(floatingActionButton,dots);
            FragmentManager fm = getSupportFragmentManager();
            assert selectedFragment != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
            setBottomButtonStyle(ToDoButton,LongTermButton,ClockButton);
            ToolbarTittle.setText(R.string.fragmentToDoToolbar);

        });
        ClockButton.setOnClickListener(v -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < mEndTime){
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            selectedFragment = new ClockFragment(floatingActionButton,dots,ToolbarTittle);
            FragmentManager fm = getSupportFragmentManager();
            assert selectedFragment != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
            setBottomButtonStyle(ClockButton,LongTermButton,ToDoButton);
            ToolbarTittle.setText(R.string.fragmentClockToolbar);

        });

        floatingActionButton.setOnClickListener(view -> {
            if (SystemClock.elapsedRealtime() - mLastClickTime < mEndTime){
                return;
            }
            mLastClickTime = SystemClock.elapsedRealtime();
            FragmentsEnumerator sF = selectedFragment.getEnum();
            switch (sF)
            {
                case LongTermHome:
                    Intent intent = new Intent(getApplicationContext(), AddLongTermActivity.class);
                    startActivity(intent);
                    break;
                case LongTermHistory:
                case ToDoHistory:
                case ClockHome:
                    showSnackBar("Add Button Disabled");
                    break;
                case ToDoHome:
                    Intent intent2 = new Intent(getApplicationContext(), AddToDoActivity.class);
                    startActivity(intent2);
                    break;
                case ClockMorningRitual:
                    Intent intent3 = new Intent(getApplicationContext(), AddMorningRitualActivity.class);
                    startActivity(intent3);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + sF);
            }
        });

        setSupportActionBar(toolbar);
    }

    @Override
    public void onBackPressed() {
        SystemExit();
    }

    public void SystemExit()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage("Are you sure, that you want to exit App?")
                .setPositiveButton("No", null)
                .setNegativeButton("Yes", (dialogInterface, i) -> System.exit(0));

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    void setBottomButtonStyle(TextView textView1, TextView textView2, TextView textView3)
    {
        textView1.setCompoundDrawableTintList(ColorStateList.valueOf(Color.parseColor("#6A00FF")));
        textView1.setTypeface(null, Typeface.BOLD);
        textView2.setCompoundDrawableTintList(ColorStateList.valueOf(Color.parseColor("#212121")));
        textView2.setTypeface(null, Typeface.NORMAL);
        textView3.setCompoundDrawableTintList(ColorStateList.valueOf(Color.parseColor("#212121")));
        textView3.setTypeface(null, Typeface.NORMAL);
    }

    void showSnackBar(String string)
    {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), string, LENGTH_SHORT);
        View v = snack.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)v.getLayoutParams();
        params.gravity = Gravity.TOP;
        v.setLayoutParams(params);
        snack.show();
    }


}
