package com.example.targethabitclock.Fragments.ToDo.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.R;

import java.util.ArrayList;

public class RecyclerViewToDoDailyAdapter extends RecyclerView.Adapter<RecyclerViewToDoDailyAdapter.ViewHolder>  {

    private Context mContext;
    private ArrayList<ToDoModel> toDoModels;
    private OnItemClickListener listener;

    public RecyclerViewToDoDailyAdapter(Context mContext, ArrayList<ToDoModel> toDoModels, OnItemClickListener listener) {
        this.mContext = mContext;
        this.toDoModels = toDoModels;
        this.listener = listener;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.todo_daily_item_view,parent,false);

        return new RecyclerViewToDoDailyAdapter.ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final ToDoModel objIncome = toDoModels.get(position);
        holder.itemView.setTag(toDoModels.get(position).getId());
        holder.task.setText(toDoModels.get(position).getTitle());

        if (listener != null) {
            listener.OnToDoClick(position, objIncome, holder.checkBox,holder.itemView,holder.bucketIMV,holder.doneIMV);
        }

    }

    @Override
    public int getItemCount() {
        return toDoModels.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
            TextView task;
            CheckBox checkBox;
            ImageView bucketIMV,doneIMV;
            ViewHolder(@NonNull View itemView)
            {
                super(itemView);
                task = itemView.findViewById(R.id.ToDoDailyTerm);
                checkBox = itemView.findViewById(R.id.rvToDoCheckBox);
                bucketIMV = itemView.findViewById(R.id.bucket);
                doneIMV = itemView.findViewById(R.id.done);

                checkBox.setChecked(false);
            }
    }

    public interface OnItemClickListener {
        void OnToDoClick(int position, ToDoModel object, CheckBox checkBox,View itemView, ImageView imageView, ImageView imageView2);
    }

}
