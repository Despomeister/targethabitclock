package com.example.targethabitclock.Fragments.LongTerm;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.Fragments.LongTerm.Adapters.RecyclerViewLongTermAdapter;
import com.example.targethabitclock.Fragments.LongTerm.Interface.LongTermDataLoadListener;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.LongTerm.ViewModel.TermViewModel;
import com.example.targethabitclock.LongTermPreviewActivity.LongTermViewActivity;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class LongTermHomeFragment extends BaseFragment implements LongTermDataLoadListener, RecyclerViewLongTermAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerViewLongTermAdapter recyclerViewLongTermAdapter;
    private TermViewModel termViewModel;

    public LongTermHomeFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_long_term_home, container, false);
        findViews(view);
        termViewModel = ViewModelProviders.of(LongTermHomeFragment.this).get(TermViewModel.class);
        getData();
        setListeners(view);
//      runLayoutAnimation(recyclerView);

        return view;
    }

    private void setListeners(final View view) {

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewLongTermAdapter);

    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.longTermRecyclerView);
    }

    @Override
    public void onTermLoaded() {
        termViewModel.getTerms().observe(this, longTermModels -> recyclerViewLongTermAdapter.notifyDataSetChanged());
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
     private void getData()
    {
        termViewModel.init(LongTermHomeFragment.this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            recyclerViewLongTermAdapter = new RecyclerViewLongTermAdapter(termViewModel.getTerms().getValue(),getContext(), this,false);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void runLayoutAnimation(RecyclerView recyclerView)
    {
        Context context = recyclerView.getContext();
        LayoutAnimationController layoutAnimationController = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(layoutAnimationController);
        Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    @Override
    public void OnTermClick(int position,LongTermModel objIncome) {

        Intent intent = new Intent(getContext(),LongTermViewActivity.class);
        intent.putExtra("Object", objIncome);
        intent.putExtra("LongTermItemStart",true);
        startActivity(intent);

    }


}