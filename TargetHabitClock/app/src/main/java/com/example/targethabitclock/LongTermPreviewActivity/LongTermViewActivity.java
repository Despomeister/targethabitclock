package com.example.targethabitclock.LongTermPreviewActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.DatabaseActivities.AddToDoActivity;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.ToDo.Adapters.RecyclerViewToDoAdapter;
import com.example.targethabitclock.Fragments.ToDo.Adapters.RecyclerViewToDoDailyAdapter;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.ViewModel.DailySuccessViewModel;
import com.example.targethabitclock.Fragments.ToDo.ViewModel.ToDoViewModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.stream.Collectors;

import me.itangqi.waveloadingview.WaveLoadingView;

public class LongTermViewActivity  extends AppCompatActivity implements ToDoDataHistoryLoadListener, ToDoDataLoadListener, RecyclerViewToDoDailyAdapter.OnItemClickListener, RecyclerViewToDoAdapter.OnItemClickListener {
    TextView descriptionTV, deadlineTV;
    LongTermModel longTermModel;
    WaveLoadingView waveLoadingView;
    TextView toolbar_tv;
    Button back,makeAsDone, addToDo;
    ImageView edit_title, edit_description;
    private RecyclerViewToDoDailyAdapter recyclerViewToDoDailyAdapter;
    private RecyclerViewToDoAdapter recyclerViewToDoAdapter;
    private RecyclerView recyclerView, recyclerViewDone;
    private ToDoViewModel toDoViewModel;
    private DailySuccessViewModel dailySuccessViewModel;
    private ArrayList<ToDoModel> toDoModels;
    int listToDoSize = 0, listDoneSize = 0;
    FirebaseUser user;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_longterm_view);
        findViews();
        getItems();
        user = FirebaseAuth.getInstance().getCurrentUser();

        toDoViewModel = ViewModelProviders.of(this).get(ToDoViewModel.class);
        toDoViewModel.init(this);


        dailySuccessViewModel = ViewModelProviders.of(this).get(DailySuccessViewModel.class);
        dailySuccessViewModel.initSuccess(this);

        setListener();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void getItems() {
        Bundle b = getIntent().getExtras();
        if(b != null)
        {
           boolean notEdited = b.getBoolean("LongTermItemStart");
           boolean edited = b.getBoolean("LongTermEditedItemStart");
           if(edited)
           {
               longTermModel = (LongTermModel) b.getSerializable("ObjectEdited");
           }
           else if(notEdited)
           {
               longTermModel = (LongTermModel) b.getSerializable("Object");
           }
        }
    }

    private void setListener() {
        descriptionTV.setText(longTermModel.getDescription());
        if(longTermModel.getDeadline().equals("Deadline unset."))
        {
            deadlineTV.setText(longTermModel.getDeadline());

        }
        else
        {
            long currentDateTime = Long.parseLong(longTermModel.getDeadline());
            Date currentDate = new Date(currentDateTime);
            @SuppressLint("SimpleDateFormat") DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            df.format(currentDate);
            deadlineTV.setText(df.format(currentDate));
        }


        String text = "Term: " + longTermModel.getTitle();
        toolbar_tv.setText(text);

        back.setOnClickListener(view -> {
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
        });

        makeAsDone.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(LongTermViewActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            builder.setMessage("Are you sure, that you want to end this term?")
                    .setPositiveButton("No", null)
                    .setNegativeButton("Yes", (dialogInterface, i) ->
                    {
                        longTermModel.setDone(true);
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        assert user != null;
                        DatabaseReference database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("LongTerm");
                        database.child(longTermModel.getId()).setValue(longTermModel);

                        Intent intent = new Intent(this, MainActivity.class);
                        startActivity(intent);
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        });

        addToDo.setOnClickListener(view -> {
            Intent intent = new Intent(this, AddToDoActivity.class);
            intent.putExtra("AddToDoFromLongTerm",true);
            intent.putExtra("ObjectLongTerm",longTermModel);
            startActivity(intent);
        });

        edit_description.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(LongTermViewActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Editing term description.");
            final EditText descriptionIn = new EditText(LongTermViewActivity.this);
            descriptionIn.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE | InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            descriptionIn.setTextColor(getResources().getColor(R.color.primary_light));
            descriptionIn.setSingleLine(false);
            descriptionIn.setImeOptions(EditorInfo.IME_FLAG_NO_ENTER_ACTION);
            descriptionIn.setText(longTermModel.getDescription());
            descriptionIn.setPadding(40,30,20,30);
            dialog.setView(descriptionIn);

            dialog.setPositiveButton("Save", (dialogInterface, i) -> {
                DatabaseReference database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("LongTerm");
                longTermModel.setDescription(descriptionIn.getText().toString());
                database.child(longTermModel.getId()).setValue(longTermModel);
                Intent intent = new Intent(this, LongTermViewActivity.class);
                intent.putExtra("Object",longTermModel);
                intent.putExtra("LongTermItemStart",true);
                startActivity(intent);
            });

            dialog.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });


        edit_title.setOnClickListener(view -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(LongTermViewActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
            dialog.setTitle("Editing term title.");
            final EditText titleIn = new EditText(LongTermViewActivity.this);
            titleIn.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_COMPLETE | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
            titleIn.setTextColor(getResources().getColor(R.color.primary_light));
            titleIn.setText(longTermModel.getTitle());
            titleIn.setPadding(40,30,20,30);
            dialog.setView(titleIn);

            dialog.setPositiveButton("Save", (dialogInterface, i) -> {
                DatabaseReference database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("LongTerm");
                longTermModel.setTitle(titleIn.getText().toString());
                database.child(longTermModel.getId()).setValue(longTermModel);
                Intent intent = new Intent(this, LongTermViewActivity.class);
                intent.putExtra("Object",longTermModel);
                intent.putExtra("LongTermItemStart",true);
                startActivity(intent);
            });

            dialog.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.cancel());
            dialog.show();
        });
    }

    private void findViews() {
        descriptionTV = findViewById(R.id.LongTermDescription);
        waveLoadingView = findViewById(R.id.waveCircleBar);
        toolbar_tv = findViewById(R.id.view_toolbar_title);
        deadlineTV = findViewById(R.id.LongTermDeadlineText);
        back = findViewById(R.id.back_button);
        makeAsDone = findViewById(R.id.make_as_done_button);
        recyclerView = findViewById(R.id.LongTermViewToDoRecyclerView);
        recyclerViewDone = findViewById(R.id.LongTermViewToDoDoneRecyclerView);
        addToDo = findViewById(R.id.longTermViewAddButton);
        edit_description = findViewById(R.id.description_edit);
        edit_title = findViewById(R.id.toolbar_title_edit);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onToDoDailySuccessLoaded() {
        ArrayList<ToDoModel> toDoDailySuccessModels = Objects.requireNonNull(dailySuccessViewModel.getToDoDailySuccess().getValue()).stream()
                .filter(o -> longTermModel.getId().equals(o.getIdLongTerm()))
                .collect(Collectors.toCollection(ArrayList::new));
        recyclerViewToDoDailyAdapter = new RecyclerViewToDoDailyAdapter(this, toDoDailySuccessModels,this);
        dailySuccessViewModel.getToDoDailySuccess().observe(this,toDoModel -> recyclerViewToDoDailyAdapter.notifyDataSetChanged());
        recyclerViewDone.setHasFixedSize(true);
        recyclerViewDone.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewDone.setAdapter(recyclerViewToDoDailyAdapter);

        toDoDailySuccessModels.forEach(o->{
            switch (o.getPriority()) {
                case "A":
                    listDoneSize += 5;
                    break;
                case "B":
                    listDoneSize += 4;
                    break;
                case "C":
                    listDoneSize += 3;
                    break;
                case "D":
                    listDoneSize += 2;
                    break;
                case "E":
                    listDoneSize += 1;
                    break;
                case "null":
                    break;
            }
        });

        double Progress;
        int ToDoTasksStack = listDoneSize + listToDoSize;
        if(listDoneSize == 0 && listToDoSize == 0)
        {
            Progress = 0.0;
        }
        else
        {
            Progress = (listDoneSize/(double) ToDoTasksStack) * 100;
        }
        waveLoadingView.setProgressValue((int)Progress);
        waveLoadingView.setCenterTitle("Progress: " + waveLoadingView.getProgressValue() + "%");

        if(toDoModels.size() == 0)
        {
            makeAsDone.setEnabled(true);
        }
        else
        {
            makeAsDone.setEnabled(false);
            makeAsDone.setTextColor(getResources().getColor(R.color.divider));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onToDoLoaded() {
        toDoModels =  Objects.requireNonNull(toDoViewModel.getToDoList().getValue()).stream()
                .filter(o-> longTermModel.getId().equals(o.getIdLongTerm()))
                .collect(Collectors.toCollection(ArrayList::new));
        recyclerViewToDoAdapter = new RecyclerViewToDoAdapter(this, toDoModels,this);
        toDoViewModel.getToDoList().observe(this,toDoModel -> recyclerViewToDoAdapter.notifyDataSetChanged());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(recyclerViewToDoAdapter);

        toDoModels.forEach(o->{
            switch (o.getPriority()) {
                case "A":
                    listToDoSize += 5;
                    break;
                case "B":
                    listToDoSize += 4;
                    break;
                case "C":
                    listToDoSize += 3;
                    break;
                case "D":
                    listToDoSize += 2;
                    break;
                case "E":
                    listToDoSize += 1;
                    break;
            }
        });
    }

    @Override
    public void OnToDoClick(int position, ToDoModel object, CheckBox checkBox, View itemView) {
        if(!toDoModels.get(position).isDone())
        {
            checkBox.setOnClickListener(view -> {
                DatabaseReference database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("ToDo");
                AlertDialog.Builder builder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Have you really done this task?")
                        .setPositiveButton("No", (dialogInterface, i) -> checkBox.setChecked(false))
                        .setNegativeButton("Yes", (dialogInterface, i) -> {

                            Date date = new Date();
                            long timeMilli = date.getTime();
                            String isDoneTime = "" + timeMilli;
                            toDoModels.get(position).setDone(true);
                            toDoModels.get(position).setDoneTime(isDoneTime);
                            database.child(toDoModels.get(position).getId()).setValue(toDoModels.get(position));

                            Intent intent = new Intent(this, LongTermViewActivity.class);
                            intent.putExtra("Object",longTermModel);
                            intent.putExtra("LongTermItemStart",true);
                            startActivity(intent);
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            });


            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(this, AddToDoActivity.class);
                intent.putExtra("Object", object);
                intent.putExtra("ObjectLT",longTermModel);
                intent.putExtra("LongTermView", true);
                startActivity(intent);
            });
        }
    }

    @Override
    public void OnToDoClick(int position, ToDoModel object, CheckBox checkBox, View itemView, ImageView bucket, ImageView done) {
        checkBox.setChecked(true);
        checkBox.setEnabled(false);
        bucket.setVisibility(View.INVISIBLE);
        done.setVisibility(View.VISIBLE);
    }
}
