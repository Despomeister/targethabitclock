package com.example.targethabitclock.Fragments.Clock.Model;

import java.io.Serializable;

public class MorningRitualModel implements Serializable {

    private String id;
    private String title;
    private String dayOfWeek;

    public void setIsDoneClickTime(String isDoneClickTime) {
        this.isDoneClickTime = isDoneClickTime;
    }

    public String getIsDoneClickTime() {
        return isDoneClickTime;
    }

    private String isDoneClickTime;
    private boolean isDone;
    private long period;

    public MorningRitualModel() {
    }

    public MorningRitualModel(String id, String title, String dayOfWeek, boolean isDone, long period, String isDoneClickTime) {
        this.id = id;
        this.title = title;
        this.dayOfWeek = dayOfWeek;
        this.isDone = isDone;
        this.period = period;
        this.isDoneClickTime = isDoneClickTime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public boolean isDone() {
        return isDone;
    }

    public long getPeriod() {
        return period;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

}
