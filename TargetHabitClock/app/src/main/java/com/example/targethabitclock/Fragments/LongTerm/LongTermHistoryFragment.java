package com.example.targethabitclock.Fragments.LongTerm;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.Fragments.LongTerm.Adapters.RecyclerViewLongTermAdapter;
import com.example.targethabitclock.Fragments.LongTerm.Interface.LongTermDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.LongTerm.ViewModel.TermHistoryViewModel;
import com.example.targethabitclock.LongTermPreviewActivity.LongTermHistoryViewActivity;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class LongTermHistoryFragment extends BaseFragment implements LongTermDataHistoryLoadListener, RecyclerViewLongTermAdapter.OnItemClickListener {
    private TermHistoryViewModel termViewModel;
    private RecyclerView recyclerView;
    private RecyclerViewLongTermAdapter recyclerViewLongTermAdapter;



    public LongTermHistoryFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_long_term_history, container, false);
        findViews(view);
        setListeners();

        termViewModel = ViewModelProviders.of(LongTermHistoryFragment.this).get(TermHistoryViewModel.class);
        getData();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewLongTermAdapter);

        return view;
    }

    private void setListeners() {
    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.longTermHistoryRecyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getData()
    {
        termViewModel.initHistory(LongTermHistoryFragment.this);
        recyclerViewLongTermAdapter = new RecyclerViewLongTermAdapter(termViewModel.getTermsHistory().getValue(),getContext(),this,true);
    }

    @Override
    public void OnTermClick(int position, LongTermModel object) {
        Intent intent = new Intent(getContext(), LongTermHistoryViewActivity.class);
        intent.putExtra("Object", object);
        startActivity(intent);
    }

    @Override
    public void onTermHistoryLoaded() {
        termViewModel.getTermsHistory().observe(this, longTermModels -> recyclerViewLongTermAdapter.notifyDataSetChanged());

    }

}