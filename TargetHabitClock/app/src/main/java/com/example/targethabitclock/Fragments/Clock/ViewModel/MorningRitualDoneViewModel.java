package com.example.targethabitclock.Fragments.Clock.ViewModel;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.Fragments.Clock.Repository.MorningRitualRepository;

import java.util.ArrayList;

public class MorningRitualDoneViewModel extends ViewModel {

    private MutableLiveData<ArrayList<MorningRitualModel>> morningRitualDoneList;
    private Fragment mFragment;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Fragment fragment)
    {
        if(morningRitualDoneList != null)
        {
            return;
        }
        mFragment = fragment;
        morningRitualDoneList = MorningRitualRepository.getInstance(fragment).getMorningRitualDone();
    }

    public LiveData<ArrayList<MorningRitualModel>> getMorningRitualDone(){
        return morningRitualDoneList;
    }
}
