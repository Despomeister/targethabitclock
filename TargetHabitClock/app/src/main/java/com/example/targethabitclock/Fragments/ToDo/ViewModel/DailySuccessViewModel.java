package com.example.targethabitclock.Fragments.ToDo.ViewModel;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.Repository.ToDoRepository;

import java.util.ArrayList;

public class DailySuccessViewModel extends ViewModel {

    private MutableLiveData<ArrayList<ToDoModel>> ToDoDailySuccess, ToDoDailySuccessToday;
    private Fragment mFragment;
    private Context mContext;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initSuccess(Fragment fragment)
    {
        if(ToDoDailySuccess != null)
        {
            return;
        }
        mFragment = fragment;
        ToDoDailySuccess = ToDoRepository.getInstance(fragment).getToDoSuccess();
        ToDoDailySuccessToday = ToDoRepository.getInstance(fragment).getToDoSuccessDaily();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initSuccess(Context context)
    {
        if(ToDoDailySuccess != null)
        {
            return;
        }
        mContext = context;
        ToDoDailySuccess = ToDoRepository.getInstance(context).getToDoSuccess();
        ToDoDailySuccessToday = ToDoRepository.getInstance(context).getToDoSuccessDaily();
    }

    public LiveData<ArrayList<ToDoModel>> getToDoDailySuccess(){return ToDoDailySuccess;}

    public LiveData<ArrayList<ToDoModel>> getToDoDailySuccessToday(){return ToDoDailySuccessToday;}

}
