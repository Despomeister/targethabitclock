package com.example.targethabitclock.Fragments.ToDo;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import com.example.targethabitclock.Base.DotsExtendedFragment;
import com.example.targethabitclock.Enumerators.FragmentsEnumerator;
import com.example.targethabitclock.Fragments.ToDo.Adapters.ViewPagerToDoAdapter;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Objects;

public class ToDoFragment extends DotsExtendedFragment {

    private ViewPager viewPager;
    private int custom_position = 0;
    private Animation rotateStart, rotateBack;


    public ToDoFragment(FloatingActionButton floatingActionButton, LinearLayout dotsLayout) {
        super(floatingActionButton, dotsLayout);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do, container, false);
        findViews(view);
        viewPager.setAdapter(new ViewPagerToDoAdapter(getFragmentManager()));

        if (savedInstanceState == null)
        {
            Bundle bundle = getArguments();
            if(bundle != null)
            {
                boolean b = this.getArguments().getBoolean("ToDoDailyStart");
                if(b)
                {
                    this.fragmentsEnumerator = FragmentsEnumerator.ToDoHistory;
                    assert getFragmentManager() != null;
                    getFragmentManager().beginTransaction().replace(R.id.view_pager_to_do, new ToDoDailySuccessFragment(floatingActionButton)).commit();
                    custom_position = 1;
                    viewPager.setCurrentItem(2);
                }

            }
            else
            {
                this.fragmentsEnumerator = FragmentsEnumerator.ToDoHome;
                assert getFragmentManager() != null;
                getFragmentManager().beginTransaction().replace(R.id.view_pager_to_do, new ToDoHomeFragment(floatingActionButton)).commit();
            }
        }



        rotateStart = AnimationUtils.loadAnimation(getContext(),R.anim.rotate_clockwise);
        rotateBack = AnimationUtils.loadAnimation(getContext(),R.anim.rotate_anticlockwise);

        addDots(custom_position++);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            @Override
            public void onPageSelected(int position) {
                if(custom_position>1)
                {
                    custom_position = 0;
                }
                addDots(custom_position++);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        return view;
    }

    private void findViews(View view) {
        viewPager = view.findViewById(R.id.view_pager_to_do);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void addDots(int currentSlidePosition)
    {

        if(dotsLayout.getChildCount() > 0)
        {
            dotsLayout.removeAllViews();
        }
        ImageView[] dots = new ImageView[2];

        for(int i =0; i < 2; i++) {
            dots[i] = new ImageView(getContext());
            if (i != currentSlidePosition)
            {
                dots[i].setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.inactive_dots));
                this.fragmentsEnumerator = FragmentsEnumerator.ToDoHome;
                this.floatingActionButton.startAnimation(rotateStart);
                this.floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#0288D1")));
            }
            else
            {
                dots[i].setImageDrawable(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.active_dot));
                this.fragmentsEnumerator = FragmentsEnumerator.ToDoHistory;
                this.floatingActionButton.startAnimation(rotateBack);
                this.floatingActionButton.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(4,0,4,0);
            dotsLayout.addView(dots[i],layoutParams);
        }
    }
}
