package com.example.targethabitclock.Base;

import android.widget.LinearLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class DotsExtendedFragment extends BaseFragment {

    protected LinearLayout dotsLayout;

    public DotsExtendedFragment(FloatingActionButton floatingActionButton,LinearLayout dotsLayout) {
        super(floatingActionButton);
        this.dotsLayout = dotsLayout;
    }
}
