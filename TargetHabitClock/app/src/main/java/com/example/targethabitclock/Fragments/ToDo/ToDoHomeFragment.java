package com.example.targethabitclock.Fragments.ToDo;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Base.BaseFragment;
import com.example.targethabitclock.DatabaseActivities.AddToDoActivity;
import com.example.targethabitclock.Enumerators.FragmentsEnumerator;
import com.example.targethabitclock.Fragments.LongTerm.Adapters.RecyclerViewLongTermAdapter;
import com.example.targethabitclock.Fragments.LongTerm.ViewModel.TermViewModel;
import com.example.targethabitclock.Fragments.ToDo.Adapters.RecyclerViewToDoAdapter;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.ViewModel.ToDoViewModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class ToDoHomeFragment extends BaseFragment implements ToDoDataLoadListener, RecyclerViewToDoAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private RecyclerViewToDoAdapter recyclerViewToDoAdapter;
    private ToDoViewModel toDoViewModel;
    private ArrayList<ToDoModel> toDoModels;
    private DatabaseReference database;



    public ToDoHomeFragment(FloatingActionButton floatingActionButton) {
        super(floatingActionButton);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_to_do_home, container, false);
        findViews(view);
        toDoViewModel = ViewModelProviders.of(ToDoHomeFragment.this).get(ToDoViewModel.class);
        getData();


        setListeners();
        return view;
    }

    private void setListeners() {
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(recyclerViewToDoAdapter);
    }

    private void findViews(View view) {
        recyclerView = view.findViewById(R.id.ToDoRecyclerView);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void getData() {
        toDoViewModel.init(ToDoHomeFragment.this);
        recyclerViewToDoAdapter = new RecyclerViewToDoAdapter(getContext(),toDoViewModel.getToDoList().getValue(), this);
        toDoModels = toDoViewModel.getToDoList().getValue();


    }

    @Override
    public void onToDoLoaded() {
        toDoViewModel.getToDoList().observe(this,toDoModel -> recyclerViewToDoAdapter.notifyDataSetChanged());
    }

    @Override
    public void OnToDoClick(int position, ToDoModel object, CheckBox checkBox,View itemView)
    {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("ToDo");
        if(!toDoModels.get(position).isDone())
        {
            checkBox.setOnClickListener(view -> {

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Have you really done this task?")
                        .setPositiveButton("No", (dialogInterface, i) -> checkBox.setChecked(false))
                        .setNegativeButton("Yes", (dialogInterface, i) -> {

                            Date date = new Date();
                            long timeMilli = date.getTime();
                            String isDoneTime = "" + timeMilli;
                            toDoModels.get(position).setDone(true);
                            toDoModels.get(position).setDoneTime(isDoneTime);
                            database.child(toDoModels.get(position).getId()).setValue(toDoModels.get(position));

                            Intent intent = new Intent(getContext(), MainActivity.class);
                            intent.putExtra("ToDoStart",true);
                            startActivity(intent);

                        });
                AlertDialog dialog = builder.create();
                dialog.show();
            });


            itemView.setOnClickListener(view -> {
                Intent intent = new Intent(getContext(), AddToDoActivity.class);
                intent.putExtra("Object", object);
                intent.putExtra("ToDoEdit",true);
                startActivity(intent);
            });
        }
    }
}