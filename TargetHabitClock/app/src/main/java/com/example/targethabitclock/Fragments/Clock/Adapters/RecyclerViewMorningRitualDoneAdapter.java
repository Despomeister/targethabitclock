package com.example.targethabitclock.Fragments.Clock.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.R;

import java.util.ArrayList;

public class RecyclerViewMorningRitualDoneAdapter extends RecyclerView.Adapter<RecyclerViewMorningRitualDoneAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<MorningRitualModel> morningRitualModels;
    private OnItemClickListener listener;

    public RecyclerViewMorningRitualDoneAdapter(Context mContext, ArrayList<MorningRitualModel> morningRitualModels, OnItemClickListener listener) {
        this.mContext = mContext;
        this.morningRitualModels = morningRitualModels;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.longterm_item_view,parent,false);

        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final MorningRitualModel objIncome = morningRitualModels.get(position);
        holder.itemView.setTag(morningRitualModels.get(position).getId());
        holder.task.setText(morningRitualModels.get(position).getTitle());
        holder.task.setTextColor(this.mContext.getColor(R.color.best));

        holder.itemView.setOnClickListener(view -> {
            if (listener != null) {
                listener.OnMorningTaskDoneClick(position, objIncome);
            }
        });

    }

    @Override
    public int getItemCount() {
        return morningRitualModels.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView task;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            task = itemView.findViewById(R.id.longterm_term);
        }
    }

    public interface OnItemClickListener {
        void OnMorningTaskDoneClick(int position, MorningRitualModel object);
    }

}
