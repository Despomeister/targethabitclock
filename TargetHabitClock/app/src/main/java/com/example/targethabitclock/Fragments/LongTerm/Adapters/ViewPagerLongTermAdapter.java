package com.example.targethabitclock.Fragments.LongTerm.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.targethabitclock.Fragments.LongTerm.LongTermHistoryFragment;
import com.example.targethabitclock.Fragments.LongTerm.LongTermHomeFragment;

public class ViewPagerLongTermAdapter extends FragmentStatePagerAdapter {

    public ViewPagerLongTermAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new LongTermHomeFragment(null);
            case 1:
                return new LongTermHistoryFragment(null);
        }
        return new LongTermHomeFragment(null);
    }

    @Override
    public int getCount() {
        return 2;
    }
}