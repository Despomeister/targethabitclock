package com.example.targethabitclock.Splash;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.example.targethabitclock.LoginActivity;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.NoInternetConnectionActivity;
import com.example.targethabitclock.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

public class SplashScreen extends AppCompatActivity {

    GoogleSignInAccount acct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        acct = GoogleSignIn.getLastSignedInAccount(this);

        Thread myThread = new Thread(){
            public void run() {
                try {
                    sleep(2000);
                    if(isNetworkConnected())
                    {
                        if(acct == null)
                            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                        else {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        }
                    }
                    else
                    {
                        startActivity(new Intent(getApplicationContext(), NoInternetConnectionActivity.class));
                    }

                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        myThread.start();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        assert cm != null;
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();


    }
}