package com.example.targethabitclock.Navigation;


import androidx.fragment.app.Fragment;

public interface NavigationListener
{
    void changeFragment(Fragment fragment, Boolean addToBackStack);
}
