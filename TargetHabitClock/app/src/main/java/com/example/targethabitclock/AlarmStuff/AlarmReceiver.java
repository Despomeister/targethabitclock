package com.example.targethabitclock.AlarmStuff;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Objects;

public class AlarmReceiver extends BroadcastReceiver {

    @SuppressLint("UnsafeProtectedBroadcastReceiver")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        String state = Objects.requireNonNull(intent.getExtras()).getString("extra");
        Intent serviceIntent = new Intent(context,RingtonePlayServices.class);
        serviceIntent.putExtra("extra", state);

        context.startService(serviceIntent);
    }
}
