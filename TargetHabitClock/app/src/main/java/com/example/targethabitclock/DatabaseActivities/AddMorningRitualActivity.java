package com.example.targethabitclock.DatabaseActivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.targethabitclock.Adapters.MySpinnerAdapter;
import com.example.targethabitclock.Entity.MorningRitual;
import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.MainActivity;
import com.example.targethabitclock.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class AddMorningRitualActivity extends AppCompatActivity {

    TextView toolbar;
    Button add,cancel;
    Spinner spinnerWithDaysOfWeek;
    AutoCompleteTextView etTitle;
    ArrayAdapter<CharSequence> arrayWithDaysOfWeekAdapter;
    FirebaseUser user;
    DatabaseReference database;
    MorningRitualModel morningRitualModel;
    String Uid;

    @SuppressLint("SetTextI18n")
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_morning_ritual);
        findViews();

        arrayWithDaysOfWeekAdapter = ArrayAdapter.createFromResource(this, R.array.days_array, R.layout.todo_spinner_item);
        user = FirebaseAuth.getInstance().getCurrentUser();
        database = FirebaseDatabase.getInstance().getReference("Users").child(Objects.requireNonNull(user.getUid())).child("MorningRitual");
        Bundle b = getIntent().getExtras();
        if(b != null) {
            if (b.getBoolean("MorningRitualEdit")) {
                Intent intent = getIntent();
                morningRitualModel = (MorningRitualModel) intent.getSerializableExtra("Object");
                add.setText("Edit");
                toolbar.setText("Editing morning ritual task");
            }
            else
                toolbar.setText("Adding new morning ritual task");
        }

        setListeners();

        if(morningRitualModel == null)
        {
            Uid = database.push().getKey();
            assert Uid != null : "database-null";
        }
        else
        {
            Uid = morningRitualModel.getId();
            etTitle.setText(morningRitualModel.getTitle());
            if (morningRitualModel.getDayOfWeek() != null) {
                switch (morningRitualModel.getDayOfWeek()) {
                    case "Monday":
                        spinnerWithDaysOfWeek.setSelection(2);
                        break;
                    case "Tuesday":
                        spinnerWithDaysOfWeek.setSelection(3);
                        break;
                    case "Wednesday":
                        spinnerWithDaysOfWeek.setSelection(4);
                        break;
                    case "Thursday":
                        spinnerWithDaysOfWeek.setSelection(5);
                        break;
                    case "Friday":
                        spinnerWithDaysOfWeek.setSelection(6);
                        break;
                    case "Saturday":
                        spinnerWithDaysOfWeek.setSelection(7);
                        break;
                    case "Sunday":
                        spinnerWithDaysOfWeek.setSelection(8);
                        break;
                    case "-":
                        spinnerWithDaysOfWeek.setSelection(1);
                        break;

                }

            }
            else
                spinnerWithDaysOfWeek.setSelection(1);
        }
    }

    @SuppressLint("SetTextI18n")
    private void setListeners() {


        arrayWithDaysOfWeekAdapter.setDropDownViewResource(R.layout.todo_dropdown_view);
        spinnerWithDaysOfWeek.setAdapter(
                new MySpinnerAdapter(
                        arrayWithDaysOfWeekAdapter,
                        R.layout.spinner_days_of_week_nothing_selected,
                        this));

        cancel.setOnClickListener(view -> ExitAlert());
        add.setOnClickListener(view -> {

            String title = etTitle.getText().toString();
            if(title.matches(""))
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(AddMorningRitualActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                builder.setMessage("Task-title cannot be empty")
                        .setPositiveButton("Ok",null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else
            {
                long period;
                String dayOfWeek = "" + spinnerWithDaysOfWeek.getSelectedItem();
                if(spinnerWithDaysOfWeek.getSelectedItem() == null || spinnerWithDaysOfWeek.getSelectedItem().equals("-"))
                {
                    period = 1;
                }
                else
                    period = 7;

                database.child(Uid).setValue(new MorningRitual(Uid,title,dayOfWeek,false,period, "null"));

                Intent intent = new Intent(AddMorningRitualActivity.this, MainActivity.class);
                intent.putExtra("MorningRitualStart",true);
                startActivity(intent);
            }
        });
    }

    private void findViews() {
        toolbar = findViewById(R.id.view_toolbar_title);
        add = findViewById(R.id.add_button_morning_ritual);
        cancel = findViewById(R.id.cancel_button_morning_ritual);
        spinnerWithDaysOfWeek = findViewById(R.id.spinnerWithDaysOfWeek);
        etTitle = findViewById(R.id.etTittle_morning_ritual);

    }

    @Override
    public void onBackPressed() {
        ExitAlert();
    }

    public void ExitAlert()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddMorningRitualActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builder.setMessage("Are you sure, that you want to back without data save?")
                .setPositiveButton("No", null)
                .setNegativeButton("Yes", (dialogInterface, i) -> finish());

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
