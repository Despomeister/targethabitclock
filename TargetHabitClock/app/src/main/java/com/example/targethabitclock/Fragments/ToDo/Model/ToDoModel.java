package com.example.targethabitclock.Fragments.ToDo.Model;

import java.io.Serializable;

public class ToDoModel implements Serializable {

    private String id, title, deadline, priority, idLongTerm, isDoneClickTime;
    private boolean done;

    public long getPeriod() {
        return period;
    }

    public ToDoModel(String id, String title, String deadline, String priority, String idLongTerm, String isDoneClickTime, boolean done, long period) {
        this.id = id;
        this.title = title;
        this.deadline = deadline;
        this.priority = priority;
        this.idLongTerm = idLongTerm;
        this.isDoneClickTime = isDoneClickTime;
        this.done = done;
        this.period = period;
    }

    private long period;

    public ToDoModel() {}

    public String getId() {
        return id;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getIsDoneClickTime() {
        return isDoneClickTime;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getTitle() {
        return title;
    }

    public String getDeadline() {
        return deadline;
    }

    public String getPriority() {
        return priority;
    }

    public String getIdLongTerm() {
        return idLongTerm;
    }

    public boolean isDone() {
        return done;
    }

    public void setDoneTime(String isDoneClickTime) {
        this.isDoneClickTime = isDoneClickTime;
    }
}
