package com.example.targethabitclock.Fragments.ToDo.Repository;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.example.targethabitclock.Entity.ToDo;
import com.example.targethabitclock.Fragments.Clock.Model.MorningRitualModel;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Interface.ToDoDataLoadListener;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class ToDoRepository {
    private static ToDoRepository instance;
    private static ToDoDataHistoryLoadListener toDoDataHistoryLoadListener;
    private ArrayList<ToDoModel> todoHistoryModels = new ArrayList<>();
    private ArrayList<ToDoModel> toDoModelsDaily = new ArrayList<>();
    private long endTime, period;
    private DatabaseReference database;

    private static ToDoDataLoadListener toDoDataLoadListener;
    private ArrayList<ToDoModel> todoModels = new ArrayList<>();

    public static ToDoRepository getInstance(Fragment fragment) {

        if(instance == null)
        {
            instance = new ToDoRepository();
        }
        if(fragment instanceof ToDoDataLoadListener)
        {
            toDoDataLoadListener = (ToDoDataLoadListener) fragment;
        }
        if(fragment instanceof ToDoDataHistoryLoadListener)
        {
            toDoDataHistoryLoadListener = (ToDoDataHistoryLoadListener) fragment;
        }
        return instance;
    }

    public static ToDoRepository getInstance(Context context) {

        if(instance == null)
        {
            instance = new ToDoRepository();
        }
        if(context instanceof ToDoDataLoadListener)
        {
            toDoDataLoadListener = (ToDoDataLoadListener) context;
        }
        if(context instanceof ToDoDataHistoryLoadListener)
        {
            toDoDataHistoryLoadListener = (ToDoDataHistoryLoadListener) context;
        }
        return instance;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<ToDoModel>> getToDo(){
        loadTerms();
        MutableLiveData<ArrayList<ToDoModel>> term = new MutableLiveData<>();
        term.setValue(todoModels);
        return term;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<ToDoModel>> getToDoSuccess(){
        loadTerms();
        MutableLiveData<ArrayList<ToDoModel>> term = new MutableLiveData<>();
        term.setValue(todoHistoryModels);

        return term;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<ToDoModel>> getToDoSuccessDaily(){
        loadTerms();
        MutableLiveData<ArrayList<ToDoModel>> term = new MutableLiveData<>();
        term.setValue(toDoModelsDaily);

        return term;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadTerms() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        Query query = reference.child("Users").child(user.getUid()).child("ToDo").orderByChild("priority");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @SuppressLint("NewApi")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                todoModels.clear();
                toDoModelsDaily.clear();
                todoHistoryModels.clear();
                for( DataSnapshot snapshot :  dataSnapshot.getChildren())
                {
                    database = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("ToDo").child(Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getId());
                    if(!Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).isDone())
                        todoModels.add(snapshot.getValue(ToDoModel.class));
                    else
                    {
                        todoHistoryModels.add(snapshot.getValue(ToDoModel.class));
                        Date date = new Date();

                            if((Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIsDoneClickTime() != null) &&
                                    (!Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIsDoneClickTime().equals("null")))
                            {
                                if (Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIdLongTerm() == null)
                                {
                                        endTime = Long.parseLong(Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIsDoneClickTime());
                                        endTime = (long) Math.floor(endTime / 86400000);
                                        period = Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getPeriod();

                                        if ((endTime * 86400000) < date.getTime() && date.getTime() < ((endTime + 1) * 86400000))
                                        {
                                            toDoModelsDaily.add(snapshot.getValue(ToDoModel.class));
                                        } else if (date.getTime() >= ((endTime + 1) * 86400000)) {
                                            if (period == 0)
                                            {
                                                if (Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIdLongTerm() == null)
                                                {
                                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                                    assert user != null;
                                                    database.removeValue();
                                                }
                                            } else if (period == 1) {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                            } else if (period == 7) {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));

                                            } else if (period == 30) {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));

                                            } else {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                            }
                                        }

                                }
                                else
                                {
                                    endTime = Long.parseLong(Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIsDoneClickTime());
                                    endTime = (long) Math.floor(endTime / 86400000);
                                    period = Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getPeriod();
                                    if (period == 0)
                                    {
                                        if ((endTime * 86400000) < date.getTime() && date.getTime() < ((endTime + 1) * 86400000))
                                        {
                                            toDoModelsDaily.add(snapshot.getValue(ToDoModel.class));
                                        }
                                    }
                                    else
                                    {
                                        if ((endTime * 86400000) < date.getTime() && date.getTime() < ((endTime + 1) * 86400000))
                                        {
                                            toDoModelsDaily.add(snapshot.getValue(ToDoModel.class));
                                        }
                                        if (date.getTime() >= ((endTime + 1) * 86400000))
                                        {
                                            if (period == 1 && ((endTime+1) * 86400000) <= date.getTime())
                                            {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                                toDoModelsDaily.remove(snapshot.getValue(ToDoModel.class));
                                            } else if (period == 7 && ((endTime+7) * 86400000) <= date.getTime()) {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                            } else if (period == 30 && ((endTime+30) * 86400000) <= date.getTime()) {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                            } else {
                                                SetCyclicTasks(snapshot.getValue(ToDoModel.class));
                                            }
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if(Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getIdLongTerm() == null)
                                {
                                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                    assert user != null;
                                    DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(user.getUid()).child("ToDo").child(Objects.requireNonNull(snapshot.getValue(ToDoModel.class)).getId());
                                    databaseReference.removeValue();
                                }
                                else
                                {
                                    todoHistoryModels.add(snapshot.getValue(ToDoModel.class));
                                }
                            }
                    }
                }
                toDoDataLoadListener.onToDoLoaded();
                toDoDataHistoryLoadListener.onToDoDailySuccessLoaded();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        query.addListenerForSingleValueEvent(valueEventListener);
    }
    private void SetCyclicTasks(ToDoModel toDoModel)
    {
            long Deadline = (endTime + period + 1) * 86400000;
            Objects.requireNonNull(toDoModel).setDeadline("" + Deadline);
            Objects.requireNonNull(toDoModel).setDone(false);
            Objects.requireNonNull(toDoModel).setDoneTime("null");
            ToDoModel obiekt = new ToDoModel(
                    Objects.requireNonNull(toDoModel).getId(),
                    Objects.requireNonNull(toDoModel).getTitle(),
                    "" + Deadline, Objects.requireNonNull(toDoModel).getPriority(),
                    Objects.requireNonNull(toDoModel).getIdLongTerm(),
                    "null", false, Objects.requireNonNull(toDoModel).getPeriod()
            );
            database.setValue(obiekt);
            todoModels.add(obiekt);
    }
}
