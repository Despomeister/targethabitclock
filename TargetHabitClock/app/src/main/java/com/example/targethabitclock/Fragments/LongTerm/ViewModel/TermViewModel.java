package com.example.targethabitclock.Fragments.LongTerm.ViewModel;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.LongTerm.Repository.LongTermRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TermViewModel extends ViewModel {

    private MutableLiveData<ArrayList<LongTermModel>> terms;
    private Fragment mFragment;
    private Context mContext;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Fragment fragment)
    {
        if(terms != null)
        {
            return;
        }
        mFragment = fragment;
        terms = LongTermRepository.getInstance(fragment).getTerms();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Context context)
    {
        if(terms != null)
        {
            return;
        }
        mContext = context;
        terms = LongTermRepository.getInstance(context).getTerms();
    }

    public LiveData<ArrayList<LongTermModel>> getTerms(){
        return terms;
    }
}
