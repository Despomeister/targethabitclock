package com.example.targethabitclock.Fragments.ToDo.Interface;

public interface ToDoDataLoadListener {

    void onToDoLoaded();
}
