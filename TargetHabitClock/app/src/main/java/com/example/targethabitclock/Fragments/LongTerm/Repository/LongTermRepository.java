package com.example.targethabitclock.Fragments.LongTerm.Repository;

import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.example.targethabitclock.Fragments.LongTerm.Interface.LongTermDataHistoryLoadListener;
import com.example.targethabitclock.Fragments.LongTerm.Interface.LongTermDataLoadListener;
import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Objects;

public class LongTermRepository {

    private static LongTermRepository instance;
    private ArrayList<LongTermModel> longTermModels = new ArrayList<>();
    private ArrayList<LongTermModel> longTermHistoryModels = new ArrayList<>();
    private static LongTermDataLoadListener dataLoadListener;
    private static LongTermDataHistoryLoadListener dataHistoryLoadListener;

    public static LongTermRepository getInstance(Fragment fragment)
    {
        if(instance == null)
        {
            instance = new LongTermRepository();
        }
        if(fragment instanceof LongTermDataLoadListener)
        {
            dataLoadListener = (LongTermDataLoadListener) fragment;
        }
        if(fragment instanceof LongTermDataHistoryLoadListener)
        {
            dataHistoryLoadListener = (LongTermDataHistoryLoadListener) fragment;
        }
        return instance;
    }

    public static LongTermRepository getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new LongTermRepository();
        }
        if(context instanceof LongTermDataLoadListener)
        {
            dataLoadListener = (LongTermDataLoadListener) context;
        }
        if(context instanceof LongTermDataHistoryLoadListener)
        {
            dataHistoryLoadListener = (LongTermDataHistoryLoadListener) context;
        }
        return instance;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<LongTermModel>> getTerms(){
        loadTerms();
        MutableLiveData<ArrayList<LongTermModel>> term = new MutableLiveData<>();
        term.setValue(longTermModels);

        return term;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public MutableLiveData<ArrayList<LongTermModel>> getTermsHistory(){
        loadTerms();
        MutableLiveData<ArrayList<LongTermModel>> term = new MutableLiveData<>();
        term.setValue(longTermHistoryModels);

        return term;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void loadTerms() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        assert user != null;
        Query query = reference.child("Users").child(user.getUid()).child("LongTerm").orderByChild("priority");
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    longTermModels.clear();
                    longTermHistoryModels.clear();
                    for( DataSnapshot snapshot :  dataSnapshot.getChildren())
                    {
                        if(!Objects.requireNonNull(snapshot.getValue(LongTermModel.class)).isDone())
                            longTermModels.add(snapshot.getValue(LongTermModel.class));
                        else
                            longTermHistoryModels.add(snapshot.getValue(LongTermModel.class));
                    }
                    dataLoadListener.onTermLoaded();
                    dataHistoryLoadListener.onTermHistoryLoaded();


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        };
        query.addListenerForSingleValueEvent(valueEventListener);
    }
}
