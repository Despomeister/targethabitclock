package com.example.targethabitclock.Fragments.ToDo.ViewModel;

import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.targethabitclock.Fragments.ToDo.Model.ToDoModel;
import com.example.targethabitclock.Fragments.ToDo.Repository.ToDoRepository;

import java.util.ArrayList;

public class ToDoViewModel extends ViewModel {

    private MutableLiveData<ArrayList<ToDoModel>> todolist;
    private Fragment mFragment;
    private Context mContext;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Fragment fragment)
    {
        if(todolist != null)
        {
            return;
        }
        mFragment = fragment;
        todolist = ToDoRepository.getInstance(fragment).getToDo();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void init(Context context)
    {
        if(todolist != null)
        {
            return;
        }
        mContext = context;
        todolist = ToDoRepository.getInstance(context).getToDo();
    }

    public LiveData<ArrayList<ToDoModel>> getToDoList(){
        return todolist;
    }
}
