package com.example.targethabitclock.Fragments.LongTerm.ViewModel;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.targethabitclock.Fragments.LongTerm.Model.LongTermModel;
import com.example.targethabitclock.Fragments.LongTerm.Repository.LongTermRepository;

import java.util.ArrayList;

public class TermHistoryViewModel extends ViewModel
{
    private MutableLiveData<ArrayList<LongTermModel>> termsHistory;
    private Fragment mFragmentHistory;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void initHistory(Fragment fragment)
    {
        if(termsHistory != null)
        {
            return;
        }
        mFragmentHistory = fragment;
        termsHistory = LongTermRepository.getInstance(fragment).getTermsHistory();
    }

    public LiveData<ArrayList<LongTermModel>> getTermsHistory(){
        return termsHistory;
    }


}
