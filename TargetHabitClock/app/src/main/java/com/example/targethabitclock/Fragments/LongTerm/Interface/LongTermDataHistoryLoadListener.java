package com.example.targethabitclock.Fragments.LongTerm.Interface;

public interface LongTermDataHistoryLoadListener {
    void onTermHistoryLoaded();
}
