package com.example.targethabitclock.AlarmStuff;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.targethabitclock.R;

import java.util.Objects;


public class RingtonePlayServices extends Service {

    private static MediaPlayer mediaPlayer = null;
    boolean isRun;
    int startId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        final NotificationManager mNM = (NotificationManager)
                getSystemService(NOTIFICATION_SERVICE);

        PendingIntent pIntent = PendingIntent.getActivity(this, 1, intent, 0);

        Notification mNotify  = new Notification.Builder(this)
                .setContentTitle("Notification start " + "!")
                .setContentText("Click me!")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pIntent)
                .setAutoCancel(false)
                .build();

        String state = Objects.requireNonNull(intent.getExtras()).getString("extra");

        if(state != null)
            switch (state) {
                case "off":
                    startId = 0;
                    break;
                case "on":
                    startId = 1;
                    System.out.println("Jesetem");
                    break;
                default:
                    startId = 0;
                    break;
            }


        if(!this.isRun && startId == 1)
        {
            if(mediaPlayer == null)
                mediaPlayer = MediaPlayer.create(this, R.raw.sea);
            mediaPlayer.start();

            assert mNM != null;
            mNM.notify(0, mNotify);

            this.isRun = true;
            this.startId = 0;

        }
        else if (!this.isRun){

            this.startId = 0;

        }

        else if (startId == 1){

            this.startId = 0;

        }
        else {

            mediaPlayer.stop();
            mediaPlayer.reset();

            this.isRun = false;
            this.startId = 0;
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this, "OnDestroy", Toast.LENGTH_LONG).show();
        super.onDestroy();
        this.isRun = false;
    }
}
