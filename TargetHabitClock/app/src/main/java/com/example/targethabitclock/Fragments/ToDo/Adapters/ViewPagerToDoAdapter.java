package com.example.targethabitclock.Fragments.ToDo.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.targethabitclock.Fragments.ToDo.ToDoDailySuccessFragment;
import com.example.targethabitclock.Fragments.ToDo.ToDoHomeFragment;

public class ViewPagerToDoAdapter extends FragmentStatePagerAdapter {

    public ViewPagerToDoAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new ToDoHomeFragment(null);
            case 1:
                return new ToDoDailySuccessFragment(null);
        }
        return new ToDoHomeFragment(null);
    }

    @Override
    public int getCount() {
        return 2;
    }
}