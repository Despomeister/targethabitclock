package com.example.targethabitclock.Enumerators;

public enum FragmentsEnumerator {
    LongTermHome, LongTermHistory,
    ToDoHome, ToDoHistory,
    ClockHome, ClockMorningRitual
}
