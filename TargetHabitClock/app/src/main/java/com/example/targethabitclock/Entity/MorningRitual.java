package com.example.targethabitclock.Entity;

public class MorningRitual {

    private String id, title, dayOfWeek,isDoneClickTime;
    private boolean isDone;
    private long period;

    public MorningRitual(){}

    public MorningRitual(String id, String title, String dayOfWeek, boolean isDone, long period, String isDoneClickTime) {
        this.id = id;
        this.title = title;
        this.dayOfWeek = dayOfWeek;
        this.isDone = isDone;
        this.period = period;
        this.isDoneClickTime = isDoneClickTime;
    }

    public String getId() {
        return id;
    }

    public String getIsDoneClickTime() {
        return isDoneClickTime;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return isDone;
    }

    public long getPeriod() {
        return period;
    }
}
